
// ================================================================================================
// -*- D -*-
// File: Main.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-06
// Brief: Application entry point.
// ================================================================================================

import deferred.ApplicationFactory;

void main(string[] cmdArgs)
{
	Application app = ApplicationFactory.createApplication();
	app.initWithCommandLine(cmdArgs);
	app.run();
	app.shutdown();
}
