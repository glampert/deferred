
// ================================================================================================
// -*- D -*-
// File: GBuffer.d
// Author: Guilherme R. Lampert
// Created on: 2015-01-04
// Brief: GBuffer interface (the "Geometry Buffer" used by deferred rendering).
// ================================================================================================

module deferred.GBuffer;

// ========================================================
// GBuffer interface:
// ========================================================

interface GBuffer
{
	// 'bind()' modes for each rendering pass:
	enum Pass
	{
		Geometry,
		Lights,
		Final,

		// Internal use.
		Count
	}

	// Render targets for each shading property
	// we need to save in the "geometry pass".
	enum GeometryPassTargets
	{
		DiffuseColor,
		NormalVectors,
		DepthBuffer,

		// Internal use.
		Count
	}

	// Render targets for each shading property
	// we need to save in the "lights pass".
	enum LightsPassTargets
	{
		EmissiveLight,
		SpecularLight,

		// Internal use.
		Count
	}

	// Sets up the G-Buffer, with the given width and height.
	void init(uint gBufferWidth, uint gBufferHeight);

	// Frees all system-side and GPU-side data.
	void freeAllData();

	// Debug visualization of the underlaying render target textures.
	void visualizeRenderTargets(uint targetWidth, uint targetHeight, uint rtWidth, uint rtHeight) const;

	// Bind the GBuffer render targets for drawing or read-back.
	void bind(Pass pass) const;

	// Restores the default framebuffer.
	void unbind() const;

	// Get the width (in pixels) of the G-Buffer textures, which was set at initialization.
	uint getWidth() const;

	// Get the height (in pixels) of the G-Buffer textures, which was set at initialization.
	uint getHeight() const;
}
