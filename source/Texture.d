
// ================================================================================================
// -*- D -*-
// File: Texture.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-23
// Brief: Texture/image interfaces.
// ================================================================================================

module deferred.Texture;

// ========================================================
// Texture interface:
// ========================================================

interface Texture
{
	// The maximum number of mip-map levels for a single physical texture.
	// This is more than enough for current-generation graphics cards.
	// A 65536x65536 (2^16) image has a maximum of 16 levels.
	static immutable uint MaxMipMapLevels = 16;

	// Texture types/usage:
	enum Type
	{
		Texture2D, // 2D texture (WxH).
		Count      // Number of entries in the enum. Internal.
	}

	// Texture addressing (or wrapping) mode:
	enum Addressing
	{
		ClampToEdge,    // Clamp (or stretch) texture to fill surface using the color of the edge pixel of the texture.
		Repeat,         // Repeat texture to fill surface.
		MirroredRepeat, // Repeat texture to fill surface, but mirrored.
		Default,        // Default addressing mode. Same as 'Repeat'.
		Count           // Number of entries in the enum. Internal.
	}

	// Texture filtering modes:
	enum Filter
	{
		Nearest,     // Nearest neighbor (or Manhattan Distance) filtering. Worst quality, best performance.
		Linear,      // Linear, mip-mapped filtering. Reasonable quality, good performance.
		Anisotropic, // Anisotropic filtering. Best quality, most expensive. Paired with an anisotropy amount.
		Default,     // Let the renderer decide.
		Count        // Number of entries in the enum. Internal.
	}

	// Internal image format:
	enum PixelFormat
	{
		Invalid = -1, // Sentinel constant. Internal use.
		RGB_U8,       // RGB  8:8:8   color
		RGBA_U8,      // RGBA 8:8:8:8 color
		BGR_U8,       // BGR  8:8:8   color
		BGRA_U8,      // BGRA 8:8:8:8 color
		Count         // Number of entries in the enum. Internal.
	}

	// All parameters needed for the creation of
	// a texture in a convenient aggregate.
	struct SetupParams
	{
		Type        type;
		Addressing  addressing;
		Filter      filter;
		PixelFormat pixelFormat;
		uint        anisotropyAmount; // From 0 to renderer.getMaxTextureAnisotropy()
		uint        numMipMapLevels;  // From 1 to MaxMipMapLevels
		uint        baseMipMapLevel;  // From 0 to numMipMapLevels-1
		uint        maxMipMapLevel;   // From 0 to numMipMapLevels-1
	}

	// Load from file. Uses default parameters.
	void loadFromFile(const string filename);

	// Load from file with user provided parameters. Throws an exception on error.
	void loadFromFile(const string filename, const ref SetupParams params);

	// Initialize from raw image data and setup parameters.
	// 'hint' should be the file extension or image type id.
	void initFromData(const ubyte[] data, const string hint, const ref SetupParams params);

	// Frees all system-side and GPU-side data.
	void freeAllData();

	// Bind/unbind for rendering.
	void bind(uint texUnit = 0) const;
	void unbind(uint texUnit = 0) const;

	// Miscellaneous queries:
	string getOriginalFileName() const;

	const(SetupParams) getDefaultParams() const;
	const(SetupParams *) getParams() const;

	Type        getType() const;
	PixelFormat getPixelFormat() const;
	Addressing  getAddressing() const;
	Filter      getFilter() const;

	uint getAnisotropyAmount() const;
	uint getNumMipMapLevels() const;
	uint getBaseMipMapLevel() const;
	uint getMaxMipMapLevel() const;
}
