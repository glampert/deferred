
// ================================================================================================
// -*- D -*-
// File: Material.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-23
// Brief: A material describes how to shade a surface.
// ================================================================================================

module deferred.Material;

public
{
	import deferred.MathUtils;
	import deferred.ShaderProgram;
	import deferred.Texture;
}

private
{
	import deferred.Common;
	static import std.algorithm;
	static import std.array;
	static import std.conv;
	static import std.file;
	static import std.string;
}

// ========================================================
// Color4 structure:
// ========================================================

struct Color4
{
	// RGBA color in [0,1] range.
	float r, g, b, a;

	// Built-in color constants:
	static immutable Color4 WhiteColor = Color4(1.0f, 1.0f, 1.0f, 1.0f);
	static immutable Color4 GrayColor  = Color4(0.5f, 0.5f, 0.5f, 1.0f);
	static immutable Color4 BlackColor = Color4(0.0f, 0.0f, 0.0f, 1.0f);
	static immutable Color4 RedColor   = Color4(1.0f, 0.0f, 0.0f, 1.0f);
	static immutable Color4 GreenColor = Color4(0.0f, 1.0f, 0.0f, 1.0f);
	static immutable Color4 BlueColor  = Color4(0.0f, 0.0f, 1.0f, 1.0f);

	this(float r, float g, float b, float a = 1.0f)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	this(const float[] rgba)
	{
		assert(rgba !is null);
		assert(rgba.length >= 4);
		this.r = rgba[0];
		this.g = rgba[1];
		this.b = rgba[2];
		this.a = rgba[3];
	}

	this(const ref Color4 other)
	{
		this.r = other.r;
		this.g = other.g;
		this.b = other.b;
		this.a = other.a;
	}

	this(const ref Vec3 v, float a = 1.0f)
	{
		this.r = v.x;
		this.g = v.y;
		this.b = v.z;
		this.a = a;
	}

	static Color4 colorForName(const string colorName)
	{
		enum namedColors = [
			"white" : WhiteColor,
			"gray"  : GrayColor,
			"black" : BlackColor,
			"red"   : RedColor,
			"green" : GreenColor,
			"blue"  : BlueColor
		];

		const Color4 invalid = Color4(-1.0f, -1.0f, -1.0f, -1.0f);
		const Color4 color   = namedColors.get(colorName, invalid);

		if (color == invalid)
		{
			Common.logWarning("No such color '", colorName, "' in built-in color table! Returning white color...");
			return WhiteColor;
		}

		return color;
	}

	void clampToColorRange()
	{
		// Makes sure the RGBA is in the [0,1] range:
		if (r < 0.0f) r = 0.0f;
		if (g < 0.0f) g = 0.0f;
		if (b < 0.0f) b = 0.0f;
		if (a < 0.0f) a = 0.0f;
		if (r > 1.0f) r = 1.0f;
		if (g > 1.0f) g = 1.0f;
		if (b > 1.0f) b = 1.0f;
		if (a > 1.0f) a = 1.0f;
	}
}

// ========================================================
// Material class:
// ========================================================

class Material
{
	private
	{
		// This will allow Material to hold const Texture objects
		// but rebind the object at any time if needed.
		import std.typecons: Rebindable;

		// Base colors:
		Color4 ambientColor;
		Color4 diffuseColor;
		Color4 specularColor;
		Color4 emmisiveColor;
		float  shininess;

		// Maps for image based lighting:
		Rebindable!(const Texture) diffuseMap;
		Rebindable!(const Texture) normalMap;
		Rebindable!(const Texture) specularMap;

		// Shader program this material will use.
		Rebindable!(const ShaderProgram) shaderProg;

		// Material name as found in file.
		string mtrName;
	}

	// ----------------------------------------------------

	this()
	{
		makeDefault();
	}

	void makeDefault()
	{
		//TODO default textures/shaderProg

		diffuseColor  = Color4.WhiteColor;
		specularColor = Color4.GrayColor;
		emmisiveColor = Color4.BlackColor;
		ambientColor  = Color4.BlackColor;
		shininess     = 50.0f;
		mtrName       = "default-material";
	}

	// Make the material current for all further geometry drawing.
	void apply() const
	{
		//TODO
	}

	// Loads the material from a material descriptor file (MTR).
	void loadFromFile(const string mtrFile)
	{
		assert(mtrFile !is null);
		assert(mtrFile.length != 0);

		Common.logComment("Loading material from file \"", mtrFile, "\"...");

		auto memory = cast(char[])std.file.read(mtrFile);
		if (memory.length == 0)
		{
			throw new Exception("MTR file was empty!");
		}

		initFromData(memory);
		Common.logComment("Successfully finished parsing material from file \"", mtrFile, "\".");
	}

	// Same as loadFromFile() but takes the already in-memory contents of an MTR or equivalent.
	void initFromData(const char[] mtrFileContents)
	{
		assert(mtrFileContents !is null);
		assert(mtrFileContents.length != 0);

		makeDefault();

		bool foundMaterialDecl    = false;
		bool expectOpeningBracket = false;
		bool expectClosingBracket = false;

		// Called when a material member is found to validate
		// the scope of the material block / make sure the token is in the right place.
		auto validateScope = (const char[] tk)
		{
			if (!foundMaterialDecl || !expectClosingBracket)
			{
				throw new Exception(std.string.format("Token '%s' found outside 'material' block!", tk));
			}
		};

		// Split into lines:
		const char[][] lines = std.string.splitLines(mtrFileContents);
		for (ulong l = 0; l < lines.length; ++l)
		{
			// Remove leading TABs/spaces:
			const char[] line = std.string.stripLeft(lines[l]);

			// Skip comment or blank lines:
			if (line.length == 0
				|| line[0] == '\n' || line[0] == '\r'
				|| std.string.startsWith(line, "//"))
			{
				continue;
			}

			// Further split the line into words/tokens:
			const char[][] tokens = std.string.split(line);
			for (ulong t = 0; t < tokens.length; ++t)
			{
				const char[] token = tokens[t];

				//
				// Simple material parsing.
				// Does only minimal validation and error checking.
				//
				if (token == "material")
				{
					if (foundMaterialDecl) // Can't have more than one!
					{
						throw new Exception("More than one 'material' block in MTR file!");
					}

					foundMaterialDecl    = true;
					expectOpeningBracket = true;
					expectClosingBracket = true;

					const string name = parseString(tokens, t);
					Common.logComment("Found material \"", name ,"\" in MTR file...");
					mtrName = name;
				}
				else if (token == "{") // This should happen after finding a "material" block.
				{
					if (!expectOpeningBracket)
					{
						throw new Exception("Lost '{' found in MTR file!");
					}
					expectOpeningBracket = false;
				}
				else if (token == "}") // Mark the end of the MTR file.
				{
					if (!expectClosingBracket)
					{
						throw new Exception("Lost '}' found in MTR file!");
					}
					expectClosingBracket = false;
				}
				else if (token == "ambientColor")
				{
					validateScope(token);
					// Skip an '=' sign:
					expectEqualSign(tokens, t);
					// Get the color:
					ambientColor = parseColor(tokens, t);
					Common.logComment("Material ambient: ", ambientColor);
				}
				else if (token == "diffuseColor")
				{
					validateScope(token);
					// Skip an '=' sign:
					expectEqualSign(tokens, t);
					// Get the color:
					diffuseColor = parseColor(tokens, t);
					Common.logComment("Material diffuse: ", diffuseColor);
				}
				else if (token == "specularColor")
				{
					validateScope(token);
					// Skip an '=' sign:
					expectEqualSign(tokens, t);
					// Get the color:
					specularColor = parseColor(tokens, t);
					Common.logComment("Material specular: ", specularColor);
				}
				else if (token == "emmisiveColor")
				{
					validateScope(token);
					// Skip an '=' sign:
					expectEqualSign(tokens, t);
					// Get the color:
					emmisiveColor = parseColor(tokens, t);
					Common.logComment("Material emmisive: ", emmisiveColor);
				}
				else if (token == "shininess")
				{
					validateScope(token);
					// Skip an '=' sign:
					expectEqualSign(tokens, t);
					// Get the number:
					shininess = parseFloat(tokens, t);
					Common.logComment("Material shininess: ", shininess);
				}
				else if (token == "diffuseMap")
				{
					validateScope(token);
					const string texName = parseString(tokens, t);
					setupTexture(texName, diffuseMap);
				}
				else if (token == "normalMap")
				{
					validateScope(token);
					const string texName = parseString(tokens, t);
					setupTexture(texName, normalMap);
				}
				else if (token == "specularMap")
				{
					validateScope(token);
					const string texName = parseString(tokens, t);
					setupTexture(texName, specularMap);
				}
			}
		}

		if (expectClosingBracket)
		{
			Common.logWarning("Missing closing '{' in MTR file!");
		}
	}

	private final void setupTexture(const string texName, const Texture texture)
	{
		Common.logComment("Found MTR texture reference: \"", texName, "\"");
		//TODO
	}

	private static const(char)[] nextToken(const char[][] tokens, ref ulong currentToken)
	{
		// Validate and advance token counter.
		++currentToken;
		if (currentToken >= tokens.length)
		{
			throw new Exception("Unexpected EOF while looking for MTR token!");
		}
		return tokens[currentToken];
	}

	private static void expectEqualSign(const char[][] tokens, ref ulong currentToken)
	{
		// Expect an equal sign '=' token and skip it.
		const char[] equalSign = nextToken(tokens, currentToken);
		if (equalSign != "=")
		{
			throw new Exception("Missing '=' after declaration!");
		}
	}

	private static string parseString(const char[][] tokens, ref ulong currentToken)
	{
		// Parse a quoted string from token. E.g.:
		//   "Who is John Galt"
		// returns the text WITHOUT quotes.

		size_t quotesFound = 0;
		const char[] quote = "\"";
		string theString;

		for (;;)
		{
			const char[] token = nextToken(tokens, currentToken);
			quotesFound += std.string.countchars(token, quote);
			if (quotesFound >= 1)
			{
				theString ~= std.string.removechars(token, quote);
				theString ~= " ";
			}
			if (quotesFound == 2)
			{
				break;
			}
		}

		return std.string.stripRight(theString);
	}

	private static Color4 parseColor(const char[][] tokens, ref ulong currentToken)
	{
		// Parses an RGBA color in the form:
		//   (r,g,b,a)
		// or a named color, from the available built-in color constants.
		// Returning a Color4 object with the color value.
		// Color values are clamped to [0,1].
		Color4 color;

		// Parse color tuple:
		const char[] token = nextToken(tokens, currentToken);
		string data = token.dup; // Need to copy because parse() advances the string
		data = std.string.stripLeft(data);
		if (data[0] == '(')
		{
			auto vals = std.conv.parse!(float[])(data, '(', ')', ',');
			color = Color4(vals);
		}
		else // Could be a color name string:
		{
			color = Color4.colorForName(data);
		}

		// Create color object, clamp it and return:
		color.clampToColorRange();
		return color;
	}

	private static float parseFloat(const char[][] tokens, ref ulong currentToken)
	{
		// Parses a (possibly) floating-point number.
		const char[] token = nextToken(tokens, currentToken);
		return std.conv.to!float(token);
	}
}
