
// ================================================================================================
// -*- D -*-
// File: ApplicationFactory.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-06
// Brief: Factory class for the Application interface.
// ================================================================================================

module deferred.ApplicationFactory;

public
{
	// The actual interface, visible by importing this file:
	import deferred.Application;
}

private
{
	// Available Application implementations (internal):
	import deferred.gl.GLFW3Application;
}

// ========================================================
// ApplicationFactory:
// ========================================================

class ApplicationFactory
{
	static Application createApplication()
	{
		return new GLFW3Application();
	}
}
