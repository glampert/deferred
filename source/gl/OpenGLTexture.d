
// ================================================================================================
// -*- D -*-
// File: OpenGLTexture.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-23
// Brief: OpenGL specific parts of the Texture interface.
// ================================================================================================

module deferred.gl.OpenGLTexture;

private
{
	import deferred.Common;
	import deferred.ImageLoader;
	import deferred.Texture;
	import deferred.gl.OpenGLRenderer;
	import derelict.opengl3.gl3;
}

// ========================================================
// OpenGLTexture:
// ========================================================

class OpenGLTexture : Texture
{
	private
	{
		GLuint         textureId;        // OpenGL texture handle.
		OpenGLRenderer renderer;         // Renderer that owns the resource.
		SetupParams    params;           // Constant texture parameters set at creation.
		string         originalFileName; // Last file loaded via a successful 'loadFromFile()'.
	}

	// ----------------------------------------------------

	this(OpenGLRenderer renderer)
	{
		assert(renderer !is null);
		this.renderer = renderer;
		this.params   = getDefaultParams();
		this.originalFileName = "";
	}

	~this()
	{
		freeAllData();
	}

	void loadFromFile(const string filename)
	{
		assert(filename !is null);
		assert(filename.length != 0);

		this.originalFileName = filename;
		this.params = getDefaultParams(); // Use defaults.

		ImageData image;
		loadImageFromFile(filename, image);
		createGLTexture(image);
	}

	void loadFromFile(const string filename, const ref SetupParams params)
	{
		assert(filename !is null);
		assert(filename.length != 0);

		this.originalFileName = filename;
		this.params = params;

		ImageData image;
		loadImageFromFile(filename, image);
		createGLTexture(image);
	}

	void initFromData(const ubyte[] data, const string hint, const ref SetupParams params)
	{
		assert(data !is null);

		this.originalFileName = "MemoryData";
		this.params = params;

		ImageData image;
		loadImageFromMemory(data, hint, image);
		createGLTexture(image);
	}

	void freeAllData()
	{
		if (textureId != 0)
		{
			unbind(0); // Ensure unbound
			glDeleteTextures(1, &textureId);
			textureId = 0;
		}

		params = getDefaultParams();
		originalFileName = "";
	}

	void bind(uint texUnit) const
	{
		// This will go thru a caching layer avoiding unnecessary state changes.
		renderer.bindTexture(this, texUnit);
	}

	void unbind(uint texUnit) const
	{
		renderer.bindNullTexture(texUnit, getGLTarget());
	}

	string getOriginalFileName() const
	{
		return originalFileName;
	}

	const(SetupParams) getDefaultParams() const
	{
		SetupParams p;
		p.type             = Texture.Type.Texture2D;
		p.addressing       = Texture.Addressing.Default;
		p.filter           = Texture.Filter.Default;
		p.pixelFormat      = Texture.PixelFormat.Invalid;
		p.anisotropyAmount = 0;
		p.numMipMapLevels  = Texture.MaxMipMapLevels;
		p.baseMipMapLevel  = 0;
		p.maxMipMapLevel   = Texture.MaxMipMapLevels - 1;
		return p;
	}

	const(SetupParams *) getParams() const
	{
		return &params;
	}

	Type getType() const
	{
		return params.type;
	}

	PixelFormat getPixelFormat() const
	{
		return params.pixelFormat;
	}

	Addressing getAddressing() const
	{
		return params.addressing;
	}

	Filter getFilter() const
	{
		return params.filter;
	}

	uint getAnisotropyAmount() const
	{
		return params.anisotropyAmount;
	}

	uint getNumMipMapLevels() const
	{
		return params.numMipMapLevels;
	}

	uint getBaseMipMapLevel() const
	{
		return params.baseMipMapLevel;
	}

	uint getMaxMipMapLevel() const
	{
		return params.maxMipMapLevel;
	}

	GLenum getGLTarget() const
	{
		return typeToGL(params.type);
	}

	GLuint getGLTextureId() const
	{
		return textureId;
	}

	private void loadImageFromFile(const string filename, ref ImageData image)
	{
		ImageLoader il = ImageLoader.createLoaderForFile(filename);
		il.loadImageFromFile(filename, image);
	}

	private void loadImageFromMemory(const ubyte[] data, const string hint, ref ImageData image)
	{
		assert(data !is null);
		assert(data.length != 0);

		ImageLoader il = ImageLoader.createLoaderForFile(hint);
		il.loadImageFromMemory(data, image);
	}

	private void refreshGLTexParams() const
	{
		// Texture must be already bound!

		const GLenum glTarget  = typeToGL(params.type);
		const GLenum glTexWrap = addressingToGL(params.addressing);

		// Addressing mode:
		glTexParameteri(glTarget, GL_TEXTURE_WRAP_S, glTexWrap);
		glTexParameteri(glTarget, GL_TEXTURE_WRAP_T, glTexWrap);

		// Filtering:
		uint anisotropyAmount;
		GLenum minFilter, magFilter;

		if (params.filter == Texture.Filter.Default)
		{
			// Use renderer values:
			renderer.getDefaultGLTexFilter(&minFilter, &magFilter, &anisotropyAmount);
		}
		else
		{
			// Use params:
			filterToGL(params.filter, (params.numMipMapLevels > 1), minFilter, magFilter);
			anisotropyAmount = params.anisotropyAmount;
		}

		glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, magFilter);

		if (anisotropyAmount > 0)
		{
			glTexParameterf(glTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, cast(GLfloat)anisotropyAmount);
		}

		// MipMap level ranges:
		assert(params.maxMipMapLevel < params.numMipMapLevels);
		glTexParameteri(glTarget, GL_TEXTURE_BASE_LEVEL, params.baseMipMapLevel);
		glTexParameteri(glTarget, GL_TEXTURE_MAX_LEVEL,  params.maxMipMapLevel);
	}

	private void createGLTexture(const ref ImageData image)
	{
		assert(image.width  != 0);
		assert(image.height != 0);
		assert(image.pixels !is null);
		assert(image.pixels.length != 0);
		assert(image.pixelFormat   != Texture.PixelFormat.Invalid);

		renderer.clearErrors();

		// Need mew id?
		if (textureId == 0)
		{
			glGenTextures(1, &textureId);
		}

		renderer.bindTexture(this, 0);

		// Image format overwrites params:
		params.pixelFormat = image.pixelFormat;

		// Convert to GL constants:
		GLenum glInternalFormat, glFormat, glType;
		pixelFormatToGL(image.pixelFormat, glInternalFormat, glFormat, glType);
		const GLenum glTarget = typeToGL(params.type);

		// Allocate first level:
		glTexImage2D(
			/* target         = */ glTarget,
			/* level          = */ 0,
			/* internalFormat = */ glInternalFormat,
			/* width          = */ image.width,
			/* height         = */ image.height,
			/* border         = */ 0,
			/* format         = */ glFormat,
			/* type           = */ glType,
			/* data           = */ image.pixels.ptr
		);

		// Refresh GL-side params:
		refreshGLTexParams();

		// Optionally generate mip-map chain:
		if (params.numMipMapLevels > 1)
		{
			glGenerateMipmap(glTarget);
		}

		renderer.checkErrors(/* throwException = */ true);
	}

	private void filterToGL(const Texture.Filter filter, const bool withMipMaps,
	                        ref GLenum minFilter, ref GLenum magFilter) const
	{
		switch (filter)
		{
		case Texture.Filter.Nearest :
			minFilter = (withMipMaps ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST);
			magFilter = GL_NEAREST;
			break;

		case Texture.Filter.Linear :
			minFilter = (withMipMaps ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
			magFilter = GL_LINEAR;
			break;

		case Texture.Filter.Anisotropic :
			// Same as trilinear, but caller also have to set GL_TEXTURE_MAX_ANISOTROPY_EXT.
			minFilter = GL_LINEAR_MIPMAP_LINEAR;
			magFilter = GL_LINEAR;
			break;

		case Texture.Filter.Default :
			// Query the renderer.
			renderer.getDefaultGLTexFilter(&minFilter, &magFilter, null);
			break;

		default :
			assert(false);
		} // End switch (filter)
	}

	private static void pixelFormatToGL(const Texture.PixelFormat pf, ref GLenum internalFormat,
	                                    ref GLenum format, ref GLenum type)
	{
		switch (pf)
		{
		case Texture.PixelFormat.RGB_U8 :
			internalFormat = GL_RGB8;
			format         = GL_RGB;
			type           = GL_UNSIGNED_BYTE;
			break;

		case Texture.PixelFormat.RGBA_U8 :
			internalFormat = GL_RGBA8;
			format         = GL_RGBA;
			type           = GL_UNSIGNED_BYTE;
			break;

		case Texture.PixelFormat.BGR_U8 :
			internalFormat = GL_RGB8;
			format         = GL_BGR;
			type           = GL_UNSIGNED_BYTE;
			break;

		case Texture.PixelFormat.BGRA_U8 :
			internalFormat = GL_RGBA8;
			format         = GL_BGRA;
			type           = GL_UNSIGNED_INT_8_8_8_8_REV;
			break;

		default :
			assert(false);
		} // End switch (pf)
	}

	private static GLenum typeToGL(const Texture.Type type)
	{
		switch (type)
		{
		case Texture.Type.Texture2D : return GL_TEXTURE_2D;
		default : assert(false);
		} // End switch (type)
	}

	private static GLenum addressingToGL(const Texture.Addressing addr)
	{
		switch (addr)
		{
		case Texture.Addressing.ClampToEdge    : return GL_CLAMP_TO_EDGE;
		case Texture.Addressing.Repeat         : return GL_REPEAT;
		case Texture.Addressing.MirroredRepeat : return GL_MIRRORED_REPEAT;
		case Texture.Addressing.Default        : return GL_REPEAT;
		default : assert(false);
		} // End switch (addr)
	}
}
