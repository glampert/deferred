
// ================================================================================================
// -*- D -*-
// File: OpenGLGBuffer.d
// Author: Guilherme R. Lampert
// Created on: 2015-01-04
// Brief: OpenGL GBuffer implementation.
// ================================================================================================

module deferred.gl.OpenGLGBuffer;

private
{
	import deferred.Common;
	import deferred.GBuffer;
	import deferred.gl.OpenGLRenderer;
	import derelict.opengl3.gl3;
}

// ========================================================
// OpenGLGBuffer:
// ========================================================

class OpenGLGBuffer : GBuffer
{
	private
	{
		// Data needed for each GBuffer pass:
		struct PassData
		{
			GLuint   fboId;                 // OpenGL Framebuffer Object.
			GLuint[] rtTextures;            // All the render target textures needed.
			GLenum[] drawBufferIds;         // GL_COLOR_ATTACHMENT* values cached for frequent use.
		}

		OpenGLRenderer renderer;            // Render that owns this GBuffer instance.
		PassData[2]    passes;              // Pass 0 is the "geometry pass", 1 is the "lights pass".
		uint           width;               // Width  of ALL renderer target textures.
		uint           height;              // Height of ALL renderer target textures.
		GLint          maxDrawBuffers;      // Value of GL_MAX_DRAW_BUFFERS.
		GLint          maxColorAttachments; // Value of GL_MAX_COLOR_ATTACHMENTS.
	}

	// ----------------------------------------------------

	this(OpenGLRenderer renderer)
	{
		assert(renderer !is null);
		this.renderer = renderer;
	}

	~this()
	{
		freeAllData();
	}

	void init(uint gBufferWidth, uint gBufferHeight)
	{
		assert(gBufferWidth  != 0);
		assert(gBufferHeight != 0);

		glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxDrawBuffers);
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxColorAttachments);

		Common.logComment("GL_MAX_DRAW_BUFFERS.......: ", maxDrawBuffers);
		Common.logComment("GL_MAX_COLOR_ATTACHMENTS..: ", maxColorAttachments);

		if (maxDrawBuffers < GeometryPassTargets.Count)
		{
			Common.fatalError("Not enough GL_DRAW_BUFFERS! Need at least ",
					cast(uint)GeometryPassTargets.Count, " for geometry pass!");
		}
		if (maxDrawBuffers < LightsPassTargets.Count)
		{
			Common.fatalError("Not enough GL_DRAW_BUFFERS! Need at least ",
					cast(uint)LightsPassTargets.Count, " for lights pass!");
		}

		width  = gBufferWidth;
		height = gBufferHeight;

		genFboAndTextures(passes[Pass.Geometry], GeometryPassTargets.Count);
		genFboAndTextures(passes[Pass.Lights],   LightsPassTargets.Count);

		initFboAndTextures(passes[Pass.Geometry], GeometryPassTargets.Count, Pass.Geometry);
		initFboAndTextures(passes[Pass.Lights],   LightsPassTargets.Count,   Pass.Lights);

		renderer.checkErrors(/* throwException = */ true);

		Common.logComment("Created GBuffer with ", cast(uint)GeometryPassTargets.Count +
				cast(int)LightsPassTargets.Count, " total renderer targets of size: ", width, "x", height);
	}

	void freeAllData()
	{
		unbind();
		for (ulong p = 0; p < passes.length; ++p)
		{
			glDeleteFramebuffers(1, &passes[p].fboId);
			glDeleteTextures(cast(GLsizei)passes[p].rtTextures.length, passes[p].rtTextures.ptr);
			passes[p].fboId         = 0;
			passes[p].rtTextures    = null;
			passes[p].drawBufferIds = null;
		}
	}

	void visualizeRenderTargets(uint targetWidth, uint targetHeight, uint rtWidth, uint rtHeight) const
	{
		glViewport(0, 0, targetWidth, targetHeight);
		uint x = 0, y = 0;

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

		// GeometryPassTargets:
		{
			glBindFramebuffer(GL_READ_FRAMEBUFFER, passes[Pass.Geometry].fboId);
			for (uint t = 0; t < GeometryPassTargets.Count; ++t)
			{
				if (t == GeometryPassTargets.DepthBuffer) // Don't attempt to draw the depth buffer.
				{
					continue;
				}
				glReadBuffer(GL_COLOR_ATTACHMENT0 + t);
				glBlitFramebuffer(0, 0, width, height, x, y, x + rtWidth, y + rtHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);
				y += rtHeight;
				if (y + rtHeight > targetHeight)
				{
					y  = 0;
					x += rtWidth;
				}
			}
		}

		// LightsPassTargets:
		{
			glBindFramebuffer(GL_READ_FRAMEBUFFER, passes[Pass.Lights].fboId);
			for (uint t = 0; t < LightsPassTargets.Count; ++t)
			{
				glReadBuffer(GL_COLOR_ATTACHMENT0 + t);
				glBlitFramebuffer(0, 0, width, height, x, y, x + rtWidth, y + rtHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);
				y += rtHeight;
				if (y + rtHeight > targetHeight)
				{
					y  = 0;
					x += rtWidth;
				}
			}
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void bind(Pass pass) const
	{
		switch (pass)
		{
		case Pass.Geometry :
			{
				glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, passes[Pass.Geometry].fboId);
				glDrawBuffers(cast(GLsizei)passes[Pass.Geometry].drawBufferIds.length,
					passes[Pass.Geometry].drawBufferIds.ptr);

				break;
			}
		case Pass.Lights :
			{
				glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, passes[Pass.Lights].fboId);
				glDrawBuffers(cast(GLsizei)passes[Pass.Lights].drawBufferIds.length,
					passes[Pass.Lights].drawBufferIds.ptr);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D,
					passes[Pass.Geometry].rtTextures[GeometryPassTargets.DepthBuffer]);

				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D,
					passes[Pass.Geometry].rtTextures[GeometryPassTargets.NormalVectors]);

				break;
			}
		case Pass.Final :
			{
				glBindFramebuffer(GL_FRAMEBUFFER, 0); // Restore the default framebuffer.

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D,
					passes[Pass.Geometry].rtTextures[GeometryPassTargets.DiffuseColor]);

				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D,
					passes[Pass.Lights].rtTextures[LightsPassTargets.EmissiveLight]);

				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D,
					passes[Pass.Lights].rtTextures[LightsPassTargets.SpecularLight]);

				break;
			}
		default :
			assert(false);
		} // End switch (pass)
	}

	void unbind() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	uint getWidth() const
	{
		return width;
	}

	uint getHeight() const
	{
		return height;
	}

	private void genFboAndTextures(ref PassData pass, uint numRenderTargets)
	{
		Common.logComment("Generating OpenGL FBO handle and GBuffer textures...");

		// FBO:
		glGenFramebuffers(1, &pass.fboId);

		// Textures:
		pass.rtTextures = new GLuint[numRenderTargets];
		glGenTextures(cast(GLsizei)numRenderTargets, pass.rtTextures.ptr);

		// Cache these since they are frequently used:
		pass.drawBufferIds = new GLenum[numRenderTargets];
		for (uint t = 0; t < numRenderTargets; ++t)
		{
			pass.drawBufferIds[t] = cast(GLenum)(GL_COLOR_ATTACHMENT0 + t);
		}
	}

	private void initFboAndTextures(ref PassData pass, uint numRenderTargets, Pass passId)
	{
		Common.logComment("Initializing OpenGL FBO and GBuffer textures for pass ", passId, "...");

		if (pass.fboId == 0)
		{
			Common.fatalError("Null FBO handle in GBuffer!");
		}

		glBindFramebuffer(GL_FRAMEBUFFER, pass.fboId);

		for (uint t = 0; t < numRenderTargets; ++t)
		{
			glBindTexture(GL_TEXTURE_2D, pass.rtTextures[t]);

			GLenum format, type, attachment;
			GLint internalFormat, filter;

			if (passId == Pass.Geometry)
			{
				geoPassGetRenderTargetTexParams(t, attachment, internalFormat, format, type, filter);
			}
			else
			{
				assert(passId == Pass.Lights);
				lightPassGetRenderTargetTexParams(t, attachment, internalFormat, format, type, filter);
			}

			// Set texture size and data format.
			// No texture data provided, just allocate the storage:
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, null);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

			// Attach texture to frame buffer:
			glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, pass.rtTextures[t], 0);
		}

		const GLenum fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (fboStatus != GL_FRAMEBUFFER_COMPLETE)
		{
			Common.fatalError("Framebuffer error: ", fboStatusToString(fboStatus));
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	private static void geoPassGetRenderTargetTexParams(uint rtIndex, ref GLenum attachment, ref GLint internalFormat,
	                                                    ref GLenum format, ref GLenum type, ref GLint filter)
	{
		switch (rtIndex)
		{
		/*
		//TODO: note, for position we can either use a float texture,
		// decode it from depth, or dived the position in the shader by the max scene
		// extents before writing (then multiply again when sampling).
		case GeometryPassTargets.Position :
			attachment     = cast(GLenum)(GL_COLOR_ATTACHMENT0 + rtIndex);
			internalFormat = GL_RGB32F;
//			internalFormat = GL_RGB;
			format         = GL_RGB;
			type           = GL_FLOAT;
//			type           = GL_UNSIGNED_BYTE;
			filter         = GL_NEAREST;
			break;
		 */

		case GeometryPassTargets.DiffuseColor :
			attachment     = cast(GLenum)(GL_COLOR_ATTACHMENT0 + rtIndex);
//			internalFormat = GL_RGB32F;
			internalFormat = GL_RGB;
			format         = GL_RGB;
//			type           = GL_FLOAT;
			type           = GL_UNSIGNED_BYTE;
			filter         = GL_NEAREST;
			break;

		case GeometryPassTargets.NormalVectors :
			attachment     = cast(GLenum)(GL_COLOR_ATTACHMENT0 + rtIndex);
//			internalFormat = GL_RGB32F;
			internalFormat = GL_RGB;
			format         = GL_RGB;
//			type           = GL_FLOAT;
			type           = GL_UNSIGNED_BYTE;
			filter         = GL_NEAREST;
			break;

		case GeometryPassTargets.DepthBuffer :
			attachment     = GL_DEPTH_ATTACHMENT;
			internalFormat = GL_DEPTH_COMPONENT24;
			format         = GL_DEPTH_COMPONENT;
			type           = GL_FLOAT;
			filter         = GL_NEAREST;
			break;

		default :
			assert(false);
		} // End switch (rtIndex)
	}

	private static void lightPassGetRenderTargetTexParams(uint rtIndex, ref GLenum attachment, ref GLint internalFormat,
	                                                      ref GLenum format, ref GLenum type, ref GLint filter)
	{
		switch (rtIndex)
		{
		case LightsPassTargets.EmissiveLight :
			attachment     = cast(GLenum)(GL_COLOR_ATTACHMENT0 + rtIndex);
//			internalFormat = GL_RGB32F;
			internalFormat = GL_RGB;
			format         = GL_RGB;
//			type           = GL_FLOAT;
			type           = GL_UNSIGNED_BYTE;
			filter         = GL_NEAREST;
			break;

		case LightsPassTargets.SpecularLight :
			attachment     = cast(GLenum)(GL_COLOR_ATTACHMENT0 + rtIndex);
//			internalFormat = GL_RGB32F;
			internalFormat = GL_RGB;
			format         = GL_RGB;
//			type           = GL_FLOAT;
			type           = GL_UNSIGNED_BYTE;
			filter         = GL_NEAREST;
			break;

		default :
			assert(false);
		} // End switch (rtIndex)
	}

	private static string fboStatusToString(GLenum fboStatus)
	{
		switch (fboStatus)
		{
		case GL_FRAMEBUFFER_COMPLETE                      : return "GL_FRAMEBUFFER_COMPLETE";
		case GL_FRAMEBUFFER_UNDEFINED                     : return "GL_FRAMEBUFFER_UNDEFINED";
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT         : return "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT : return "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER        : return "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER";
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER        : return "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER";
		case GL_FRAMEBUFFER_UNSUPPORTED                   : return "GL_FRAMEBUFFER_UNSUPPORTED";
		case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE        : return "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE";
		case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS      : return "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS";
		default                                           : return "Unknown Framebuffer status";
		} // End switch (fboStatus)
	}
}
