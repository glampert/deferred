
// ================================================================================================
// -*- D -*-
// File: OpenGLRenderer.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: OpenGL Renderer implementation.
// ================================================================================================

module deferred.gl.OpenGLRenderer;

private
{
	import deferred.Application;
	import deferred.Common;
	import deferred.GBuffer;
	import deferred.Model3d;
	import deferred.Renderer;
	import deferred.ShaderProgram;
	import deferred.Texture;
	import deferred.gl.OpenGLGBuffer;
	import deferred.gl.OpenGLModel3d;
	import deferred.gl.OpenGLShaderProgram;
	import deferred.gl.OpenGLTexture;
	import derelict.opengl3.gl3;
}

// ========================================================
// OpenGLRenderer:
// ========================================================

class OpenGLRenderer : Renderer
{
	private
	{
		// Data used to draw a fullscreen NDC
		// quadrilateral (for pos-processing):
		GLuint        screenQuadVAO;    // Modern OpenGL requires a VAO for everything.
		GLuint        screenQuadVBO;    // VBO with vertexes for a fullscreen NDC quadrilateral.
		ShaderProgram screenQuadShader; // Shader used to draw the fullscreen NDC quadrilateral.

		// Light meshes:
		Model3d pointLightMesh;	        // A low-poly sphere.

		// Miscellaneous:
		LaunchOptions launchOptions;    // Startup command line options.
		string        shaderSourcePath; // Path where to look for shader files and #includes. Empty by default.
		string        glslVersionTag;   // Proper #version tag to be added to shaders.
	}

	// ----------------------------------------------------

	ShaderProgram createShaderProgram()
	{
		return new OpenGLShaderProgram(this);
	}

	Texture createTexture()
	{
		return new OpenGLTexture(this);
	}

	Model3d createModel3d()
	{
		return new OpenGLModel3d(this);
	}

	GBuffer createGBuffer()
	{
		return new OpenGLGBuffer(this);
	}

	void initWithOptions(const ref LaunchOptions options)
	{
		Common.logComment("Initializing OpenGL renderer...");

		shaderSourcePath = "";
		launchOptions = options;

		parseGLSLVersion();
		setDefaultGLStates();

		// Cache these in advance:
		pointLightMesh = createModel3d();
		pointLightMesh.makeSphereModel(14, 14, 1.0f, MeshBufferStorage.Static);

		Common.logComment("OpenGL renderer initialization completed!");
	}

	void shutdown()
	{
		if (screenQuadVAO != 0)
		{
			glDeleteVertexArrays(1, &screenQuadVAO);
			screenQuadVAO = 0;
		}
		if (screenQuadVBO != 0)
		{
			glDeleteBuffers(1, &screenQuadVBO);
			screenQuadVBO = 0;
		}
		if (screenQuadShader !is null)
		{
			screenQuadShader.freeAllData();
			screenQuadShader = null;
		}
		if (pointLightMesh !is null)
		{
			pointLightMesh.freeAllData();
			pointLightMesh = null;
		}
	}

	const(LaunchOptions *) getLaunchOptions() const
	{
		return &launchOptions;
	}

	uint getMaxTextureAnisotropy() const
	{
		return 8; //TODO query the GL instead!
	}

	void setShaderSourcePath(const string path)
	{
		shaderSourcePath = path;
	}

	string getShaderSourcePath() const
	{
		return shaderSourcePath;
	}

	string getGLSLVersionTag() const
	{
		return glslVersionTag;
	}

	void clearErrors() const
	{
		while (glGetError() != GL_NO_ERROR) { }
	}

	void checkErrors(bool throwException) const
	{
		const GLenum errCode = glGetError();
		if (errCode != GL_NO_ERROR)
		{
			const auto msg = std.string.format("glGetError() returned: '%s' (%d)!", GLErrCodeStr(errCode), errCode);
			Common.logWarning(msg);
			if (throwException)
			{
				throw new Exception(msg);
			}
		}
	}

	void getDefaultGLTexFilter(GLenum * minFilter, GLenum * magFilter, uint * anisotropyAmount) const
	{
		if (minFilter !is null)
		{
			//TODO temp
			*minFilter = GL_LINEAR_MIPMAP_LINEAR;
		}
		if (magFilter !is null)
		{
			//TODO temp
			*magFilter = GL_LINEAR;
		}
		if (anisotropyAmount !is null)
		{
			//TODO temp
			*anisotropyAmount = 0;
		}
	}

	const(Model3d) getLightMesh(in Light light) const
	{
		assert(light !is null);

		switch (light.lightType)
		{
		case Light.Type.Point :
			return pointLightMesh;

		default :
			assert(false);
		} // End switch (light.lightType)
	}

	void bindShaderProgram(in OpenGLShaderProgram program) const
	{
		assert(program !is null);
		glUseProgram(program.getGLProgramId());
	}

	void bindNullShaderProgram() const
	{
		glUseProgram(0);
	}

	void bindTexture(in OpenGLTexture texture, uint texUnit) const
	{
		assert(texture !is null);
		glActiveTexture(GL_TEXTURE0 + texUnit);
		glBindTexture(texture.getGLTarget(), texture.getGLTextureId());
	}

	void bindNullTexture(uint texUnit, GLenum target) const
	{
		glActiveTexture(GL_TEXTURE0 + texUnit);
		glBindTexture(target, 0);
	}

	void drawFullscreenQuadrilateral(in ShaderProgram program = null)
	{
		// Only created once:
		if (screenQuadVBO == 0)
		{
			// Set up the default shader program:
			{
				screenQuadShader = createShaderProgram();
				screenQuadShader.loadFromFile("ndc_quad.vert", "ndc_quad.frag");
				screenQuadShader.bind();

				const int  tmu = 0;
				const auto textureSampler = screenQuadShader.getShaderUniformHandle("u_texture_samp");
				screenQuadShader.setShaderUniform(textureSampler, tmu);
			}

			// Set up the Vertex Buffer and VAO:
			//
			immutable float[] verts = [
				// First triangle:
				 1.0,  1.0,
				-1.0,  1.0,
				-1.0, -1.0,
				// Second triangle:
				-1.0, -1.0,
				 1.0, -1.0,
				 1.0,  1.0
			];

			glGenVertexArrays(1, &screenQuadVAO);
			glGenBuffers(1, &screenQuadVBO);

			glBindVertexArray(screenQuadVAO);
			glBindBuffer(GL_ARRAY_BUFFER, screenQuadVBO);

			glBufferData(GL_ARRAY_BUFFER, verts.length * float.sizeof, verts.ptr, GL_STATIC_DRAW);

			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, null);

			glBindVertexArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			checkErrors(/* throwException = */ true);

			Common.logComment("Created VAO & VBO for a fullscreen quadrilateral...");
		}

		assert(screenQuadShader !is null);
		assert(screenQuadVBO != 0);
		assert(screenQuadVAO != 0);

		const ShaderProgram shaderProg = (program is null) ? screenQuadShader : program;

		shaderProg.bind();
		glDisable(GL_DEPTH_TEST);

		// Draw 6 vertexes => 2 triangles:
		glBindVertexArray(screenQuadVAO);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);

		glEnable(GL_DEPTH_TEST);
		shaderProg.unbind();
	}

	private void parseGLSLVersion()
	{
		import core.stdc.stdio; // for 'sscanf()'

		// Parse GLSL version from string:
		const char * glslVersionStr = glGetString(GL_SHADING_LANGUAGE_VERSION);
		if (glslVersionStr is null)
		{
			throw new Exception("glGetString(GL_SHADING_LANGUAGE_VERSION) returned null!");
		}

		// Try to get the GLSL version: <MAJOR.MINOR>
		GLint major = 0, minor = 0;
		if (sscanf(glslVersionStr, "%d.%d", &major, &minor) != 2)
		{
			throw new Exception("Failed to parse GLSL version numbers!");
		}

		glslVersionTag = std.string.format("#version %d\n", (major * 100) + minor);
	}

	private void setDefaultGLStates()
	{
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDisable(GL_SCISSOR_TEST);
		glDisable(GL_BLEND);

		glDepthMask(GL_TRUE);
		glDepthFunc(GL_LESS);
		glEnable(GL_DEPTH_TEST);

		glFrontFace(GL_CCW);
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);

		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);

		glPixelStorei(GL_PACK_ALIGNMENT,   1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	}

	private static string GLErrCodeStr(GLenum errCode)
	{
		switch (errCode)
		{
		case GL_NO_ERROR                      : return "GL_NO_ERROR";
		case GL_INVALID_ENUM                  : return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE                 : return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION             : return "GL_INVALID_OPERATION";
		case GL_INVALID_FRAMEBUFFER_OPERATION : return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_OUT_OF_MEMORY                 : return "GL_OUT_OF_MEMORY";
		default                               : return "Unknown OpenGL error";
		} // End switch (errCode)
	}
}
