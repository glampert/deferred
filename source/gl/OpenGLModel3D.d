
// ================================================================================================
// -*- D -*-
// File: OpenGLModel3d.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: OpenGL specific parts of the Model3d interface.
// ================================================================================================

module deferred.gl.OpenGLModel3d;

private
{
	import deferred.Common;
	import deferred.Model3d;
	import deferred.Model3dImporter;
	import deferred.gl.OpenGLRenderer;
	import derelict.opengl3.gl3;
	static import std.array;
	static import std.string;
}

// ========================================================
// OpenGLModel3d:
// ========================================================

class OpenGLModel3d : Model3d
{
	private
	{
		GLuint         vbId;             // GL Vertex Buffer handle.
		GLuint         ibId;             // GL Index Buffer handle.
		GLuint         vaoId;            // VAO handle.
		OpenGLRenderer renderer;         // Renderer that owns the resource.
		SubMesh3d[]    meshes;           // Sub-meshes that compose this model. Should always have at least 1.
		uint           vbVertexCount;    // Number of vertexes for all meshes.
		uint           ibIndexCount;     // Number of indexes for all meshes.
		string         originalFileName; // Last file loaded via a successful 'loadFromFile()'.
	}

	// ----------------------------------------------------

	this(OpenGLRenderer renderer)
	{
		assert(renderer !is null);
		this.renderer = renderer;
		this.originalFileName = "";
	}

	~this()
	{
		freeAllData();
	}

	void loadFromFile(const string filename, MeshBufferStorage storage)
	{
		Model3dImporter.importModel3dFromFile(filename, this);
		originalFileName = filename; // Remember file that originated this model.
		refreshGpuBuffers(storage);
	}

	void initFromData(SubMesh3d[] meshSet, MeshBufferStorage storage, const string name)
	{
		assert(meshSet !is null);

		meshes = meshSet;
		originalFileName = name;

		refreshGpuBuffers(storage);
	}

	void makeSphereModel(uint numZSamples, uint numRadialSamples, float radius, MeshBufferStorage storage)
	{
		DrawVertex[] verts;
		Triangle[]   triangles;
		Model3dImporter.createSphere(numZSamples, numRadialSamples, radius, verts, triangles);

		auto meshSet = new SubMesh3d[1];
		meshSet[0].indexes  = new uint[triangles.length * 3];
		meshSet[0].vertexes = verts;

		for (ulong t = 0; t < triangles.length; ++t)
		{
			const(Triangle *) tri = &triangles[t];
			meshSet[0].indexes[(t * 3) + 0] = tri.vertIndex[0];
			meshSet[0].indexes[(t * 3) + 1] = tri.vertIndex[1];
			meshSet[0].indexes[(t * 3) + 2] = tri.vertIndex[2];
		}

		meshSet[0].firstVertexInVB = 0;
		meshSet[0].firstIndexInIB  = 0;

		initFromData(meshSet, storage, "sphere");
	}

	void refreshGpuBuffers(MeshBufferStorage storage)
	{
		if (meshes.length == 0)
		{
			Common.logWarning("Model \"", originalFileName ,"\" has no meshes!");
			return;
		}

		// Count max vertexes/indexes and allocate memory:
		uint vertexCount = 0;
		uint indexCount  = 0;

		DrawVertex[] allVertexes;
		uint[] allIndexes;

		for (ulong m = 0; m < meshes.length; ++m)
		{
			const(SubMesh3d *) mesh = &meshes[m];

			vertexCount += mesh.vertexes.length;
			indexCount  += mesh.indexes.length;

			// Copy vertexes:
			for (ulong v = 0; v < mesh.vertexes.length; ++v)
			{
				allVertexes ~= mesh.vertexes[v];
			}

			// Copy indexes adding start offset so that
			// we will be able to render without glDrawElementsBaseVertex.
			for (ulong i = 0; i < mesh.indexes.length; ++i)
			{
				const uint idx = mesh.indexes[i] + mesh.firstVertexInVB;
				allIndexes ~= idx;
			}
		}

		if (vertexCount == 0 || indexCount == 0)
		{
			Common.logWarning("Model \"", originalFileName, "\" has no vertexes or indexes!");
			return;
		}

		// Gen buffer handles once:
		if (vbId  == 0) { glGenBuffers(1, &vbId); }
		if (ibId  == 0) { glGenBuffers(1, &ibId); }
		if (vaoId == 0) { glGenVertexArrays(1, &vaoId); }

		assert(vbId  != 0);
		assert(ibId  != 0);
		assert(vaoId != 0);

		vbVertexCount = vertexCount;
		ibIndexCount  = indexCount;
		const GLenum glStorage = meshBufferStorageToGL(storage);

		// Bind:
		glBindVertexArray(vaoId);
		glBindBuffer(GL_ARRAY_BUFFER, vbId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibId);

		// Actually allocate storage:
		glBufferData(GL_ARRAY_BUFFER, (vbVertexCount * DrawVertex.sizeof), allVertexes.ptr, glStorage); // VB
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (ibIndexCount * uint.sizeof), allIndexes.ptr, glStorage); // IB

		// Set "vertex format":
		glEnableVertexAttribArray(DrawVertex.PositionAttrIndex);
		glVertexAttribPointer(DrawVertex.PositionAttrIndex, 3, GL_FLOAT,
				GL_FALSE, DrawVertex.sizeof, cast(GLvoid *)DrawVertex.PositionOffset);

		glEnableVertexAttribArray(DrawVertex.NormalAttrIndex);
		glVertexAttribPointer(DrawVertex.NormalAttrIndex, 3, GL_FLOAT,
				GL_FALSE, DrawVertex.sizeof, cast(GLvoid *)DrawVertex.NormalOffset);

		glEnableVertexAttribArray(DrawVertex.TangentAttrIndex);
		glVertexAttribPointer(DrawVertex.TangentAttrIndex, 3, GL_FLOAT,
				GL_FALSE, DrawVertex.sizeof, cast(GLvoid *)DrawVertex.TangentOffset);

		glEnableVertexAttribArray(DrawVertex.BiTangentAttrIndex);
		glVertexAttribPointer(DrawVertex.BiTangentAttrIndex, 3, GL_FLOAT,
				GL_FALSE, DrawVertex.sizeof, cast(GLvoid *)DrawVertex.BiTangentOffset);

		glEnableVertexAttribArray(DrawVertex.TexCoordsAttrIndex);
		glVertexAttribPointer(DrawVertex.TexCoordsAttrIndex, 2, GL_FLOAT,
				GL_FALSE, DrawVertex.sizeof, cast(GLvoid *)DrawVertex.TexCoordsOffset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		Common.logComment("GL buffers created for Model3d \"", originalFileName, "\"...");
	}

	void freeAllData()
	{
		meshes.clear();
		vbVertexCount    = 0;
		ibIndexCount     = 0;
		originalFileName = "";

		if (vbId != 0)
		{
			glDeleteBuffers(1, &vbId);
			vbId = 0;
		}
		if (ibId != 0)
		{
			glDeleteBuffers(1, &ibId);
			ibId = 0;
		}
		if (vaoId != 0)
		{
			glDeleteVertexArrays(1, &vaoId);
			vaoId = 0;
		}
	}

	void draw() const
	{
		glBindVertexArray(vaoId);

		// Render whole VB from the start:
		glDrawElements(GL_TRIANGLES, cast(GLsizei)ibIndexCount, GL_UNSIGNED_INT, null);

		/*TODO
		// Render every mesh:
		for (ulong m = 0; m < meshes.length; ++m)
		{
			const(SubMesh3d *) mesh = &meshes[m];

			glDrawElements(GL_TRIANGLES, cast(GLsizei)mesh.indexes.length,
					GL_UNSIGNED_INT, cast(GLvoid *)(mesh.firstIndexInIB * uint.sizeof));
		}
		*/

		glBindVertexArray(0);
	}

	uint getVertexCount() const
	{
		return vbVertexCount;
	}

	uint getIndexCount() const
	{
		return ibIndexCount;
	}

	ulong getVertexBufferSizeBytes() const
	{
		return vbVertexCount * DrawVertex.sizeof;
	}

	ulong getIndexBufferSizeBytes() const
	{
		return ibIndexCount * uint.sizeof;
	}

	void allocateSubMeshes(uint num)
	{
		if (num == 0)
		{
			meshes.clear();
		}
		else
		{
			meshes.length = num;
		}
	}

	uint geSubMeshCount() const
	{
		return cast(uint)meshes.length;
	}

	SubMesh3d * getSubMesh(uint index)
	{
		assert(index < cast(uint)meshes.length);
		return &meshes[index];
	}

	const(SubMesh3d *) getSubMesh(uint index) const
	{
		assert(index < cast(uint)meshes.length);
		return &meshes[index];
	}

	string getOriginalFileName() const
	{
		return originalFileName;
	}

	private static GLenum meshBufferStorageToGL(MeshBufferStorage storage)
	{
		switch (storage)
		{
		case MeshBufferStorage.Static  : return GL_STATIC_DRAW;
		case MeshBufferStorage.Dynamic : return GL_DYNAMIC_DRAW;
		case MeshBufferStorage.Stream  : return GL_STREAM_DRAW;
		default : assert(false);
		} // End switch (storage)
	}
}
