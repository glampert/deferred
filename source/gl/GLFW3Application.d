
// ================================================================================================
// -*- D -*-
// File: GLFW3Application.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-06
// Brief: Implementation of the Application interface using GLFW v3 for window management.
// ================================================================================================

module deferred.gl.GLFW3Application;

private
{
	import deferred.Application;
	import deferred.Camera;
	import deferred.Common;
	import deferred.gl.OpenGLRenderer;
	import derelict.glfw3.glfw3;
	import derelict.opengl3.gl3;
	static import std.conv;
	static import std.getopt;
	static import std.string;
}

// ========================================================
// GLFW3Application:
// ========================================================

class GLFW3Application : Application
{
	private
	{
		// Default window size if the user doesn't provide one via the command line.
		enum DefaultWindowSize
		{
			Width  = 1024,
			Height = 768
		}

		// Title displayed in the app menu bar.
		static immutable string appTitle = "deferreD (OpenGL)";

		// Default clear screen color (passed to glClearColor).
		static immutable float[] clearColor = [ 0.25f, 0.1f, 0.0f, 1.0f ];

		// Member variables:
		OpenGLRenderer renderer;      // The OpenGL renderer back-end.
		GLFWwindow *   glWindow;      // GLFW window handle.
		Camera         camera;        // First-person camera for scene navigation.
		LaunchOptions  launchOptions; // Startup command line options.
	}

	// ----------------------------------------------------

	void initWithCommandLine(string[] cmdArgs)
	{
		Common.logComment("------------ deferreD application starting up... ------------");

		// Make it true by default.
		// If the '--verbose' param is not present in the cmd line, it will remain true.
		launchOptions.verbose = true;

		// Parse the command line:
		try
		{
			std.getopt.getopt(cmdArgs,
				"winWidth"   , &launchOptions.windowWidth,       // Optional; defaults to DefaultWindowSize.Width
				"winHeight"  , &launchOptions.windowHeight,      // Optional; defaults to DefaultWindowSize.Height
				"numSamples" , &launchOptions.numSamples,        // Optional; defaults to 0 (multisampling disabled)
				"vsync"      , &launchOptions.vsync,             // Optional; defaults to 'false'
				"fullscreen" , &launchOptions.runningFullScreen, // Optional; defaults to 'false'
				"verbose"    , &launchOptions.verbose            // Optional; defaults to 'true'
			);
		}
		catch (Exception e)
		{
			Common.logWarning("Problems parsing command line: ", Common.oneliner(e.toString()));
		}

		Common.setLogVerbosity(
			/* printComments = */ launchOptions.verbose,
			/* printWarnings = */ true,
			/* printErrors   = */ true
		);

		if (launchOptions.windowWidth == 0)
		{
			launchOptions.windowWidth = DefaultWindowSize.Width;
		}
		if (launchOptions.windowHeight == 0)
		{
			launchOptions.windowHeight = DefaultWindowSize.Height;
		}

		Common.logComment("opt window width....: ", launchOptions.windowWidth);
		Common.logComment("opt window height...: ", launchOptions.windowHeight);
		Common.logComment("opt num samples.....: ", launchOptions.numSamples);
		Common.logComment("opt v-sync..........: ", launchOptions.vsync);
		Common.logComment("opt run fullscreen..: ", launchOptions.runningFullScreen);
		Common.logComment("opt verbose.........: ", launchOptions.verbose);

		// Finish initialization:
		initGLFW();
		initOpenGL();

		// Set up the camera:
		camera = new Camera();
		camera.setViewport(launchOptions.windowWidth, launchOptions.windowHeight);
		camera.setEyeOrigin(0.0f, 0.0f, -20.0f);
	}

	void renderScene(ShaderProgram shader, Model3d model)
	{
		shader.bind();
		const Mat4 viewProj = camera.getViewProjectionMatrix();

		float j = -3.0f;
		for (int i = 0; i < 6; ++i)
		{
			Mat4 modelMatrix;
			modelMatrix.setTranslation(j * 2.0f, 0.0f, -8.0f);

			Mat4 mvpMatrix;
			mvpMatrix = Mat4.mul(viewProj, modelMatrix); //p*v*m

			shader.setShaderUniform(
				shader.getShaderUniformHandle("u_mvp_matrix"), mvpMatrix);

			model.draw();

			j += 1.0f;
		}

		j = -1.5f;
		for (int i = 0; i < 5; ++i)
		{
			Mat4 modelMatrix;
			modelMatrix.setTranslation(j * 2.0f, 2.0f, -9.5f);

			Mat4 mvpMatrix;
			mvpMatrix = Mat4.mul(viewProj, modelMatrix); //p*v*m

			shader.setShaderUniform(
				shader.getShaderUniformHandle("u_mvp_matrix"), mvpMatrix);

			model.draw();

			j += 1.0f;
		}

		shader.unbind();
	}

	static immutable uint TotalLights = 2;
	Light[] lights;

	void renderLights(ShaderProgram shader)
	{
		shader.bind();

		const Mat4 viewProj = camera.getViewProjectionMatrix();

		foreach(const Light light; lights)
		{
			Mat4 modelMatrix;
			const Vec3 lpos = light.worldPosition;
			modelMatrix.setTranslation(lpos);

			Mat4 scale;
			scale.setScale(light.pointRadius, light.pointRadius, light.pointRadius);

			modelMatrix = Mat4.mul(modelMatrix, scale);

			Mat4 mvpMatrix;
			mvpMatrix = Mat4.mul(viewProj, modelMatrix); //p*v*m

			shader.setShaderUniform(
				shader.getShaderUniformHandle("u_mvp_matrix"), mvpMatrix);

			shader.setShaderUniform(
				shader.getShaderUniformHandle("u_viewproj_matrix"), viewProj);

			const Vec3 campos = camera.getEyeOrigin();
			shader.setShaderUniform(
				shader.getShaderUniformHandle("u_camera_world_pos"), campos);

			//TODO state caching!
			const float radius = light.pointRadius;
			const float dist = lpos.distance(campos);
			if (dist < radius)
			{
				//camera is inside the light volume
				glCullFace(GL_FRONT);
			}
			else
			{
				glCullFace(GL_BACK);
			}

			renderer.getLightMesh(light).draw();
		}

		shader.unbind();
	}

	void run()
	{
		//TEMP
		import std.stdio;

		float lx = 0;
		lights = new Light[TotalLights];
		foreach(ref Light light; lights)
		{
			light = new Light();
			light.lightType     = Light.Type.Point;
			light.lightColor    = Color4.WhiteColor;
			light.worldPosition = Vec3(lx * 4.5f, 2.5, -8.5f);
			light.setPointLightRadiusAndDeriveAttenuations(2.5f);
			lx += 1.0f;
		}

		GBuffer gbuffer = renderer.createGBuffer();
		gbuffer.init(launchOptions.windowWidth, launchOptions.windowHeight);

		ShaderProgram geometryPassShader = renderer.createShaderProgram();
		{
			geometryPassShader.loadFromFile("deferred_geometry_pass.vert", "deferred_geometry_pass.frag");

			const ShaderUniformHandle u_mvp_matrix = geometryPassShader.getShaderUniformHandle("u_mvp_matrix");
			assert(u_mvp_matrix != -1);

			const ShaderUniformHandle u_diffuse_samp = geometryPassShader.getShaderUniformHandle("u_diffuse_samp");
			assert(u_diffuse_samp != -1);

			const ShaderUniformHandle u_normal_samp = geometryPassShader.getShaderUniformHandle("u_normal_samp");
			assert(u_normal_samp != -1);

			int tmu;
			geometryPassShader.bind();

			tmu = 0;
			geometryPassShader.setShaderUniform(u_diffuse_samp, tmu);

			tmu = 1;
			geometryPassShader.setShaderUniform(u_normal_samp, tmu);
		}

		ShaderProgram lightsPassShader = renderer.createShaderProgram();
		{
			lightsPassShader.loadFromFile("deferred_lights_pass.vert", "deferred_lights_pass.frag");

			const ShaderUniformHandle u_depth_samp = lightsPassShader.getShaderUniformHandle("u_depth_samp");
			assert(u_depth_samp != -1);

			const ShaderUniformHandle u_normal_samp = lightsPassShader.getShaderUniformHandle("u_normal_samp");
			assert(u_normal_samp != -1);

			int tmu;
			lightsPassShader.bind();

			tmu = 0;
			lightsPassShader.setShaderUniform(u_depth_samp, tmu);

			tmu = 1;
			lightsPassShader.setShaderUniform(u_normal_samp, tmu);
		}

		ShaderProgram finalPassShader = renderer.createShaderProgram();
		{
			finalPassShader.loadFromFile("ndc_quad.vert", "deferred_final_pass.frag");

			const ShaderUniformHandle u_diffuse_color_samp = finalPassShader.getShaderUniformHandle("u_diffuse_color_samp");
			assert(u_diffuse_color_samp != -1);

			const ShaderUniformHandle u_light_emissive_samp = finalPassShader.getShaderUniformHandle("u_light_emissive_samp");
			assert(u_light_emissive_samp != -1);

			const ShaderUniformHandle u_light_specular_samp = finalPassShader.getShaderUniformHandle("u_light_specular_samp");
			assert(u_light_specular_samp != -1);

			int tmu;
			finalPassShader.bind();

			tmu = 0;
			finalPassShader.setShaderUniform(u_diffuse_color_samp, tmu);

			tmu = 1;
			finalPassShader.setShaderUniform(u_light_emissive_samp, tmu);

			tmu = 2;
			finalPassShader.setShaderUniform(u_light_specular_samp, tmu);
		}

		Model3d mdl = renderer.createModel3d();
		mdl.loadFromFile("cube.obj", MeshBufferStorage.Static);
		//mdl.makeSphereModel(12, 12, 2.0f, MeshBufferStorage.Static);

		Texture diffuseMap = renderer.createTexture();
		Texture normalMap  = renderer.createTexture();

		Material mtr = new Material();
		try
		{
			diffuseMap.loadFromFile("spnza_bricks_a_diff.tga");
			normalMap.loadFromFile("spnza_bricks_a_ddn.tga");

			mtr.loadFromFile("test_material.mtr");
		}
		catch (Exception e)
		{
			Common.logError(Common.oneliner(e.toString()));
		}

		scope(exit)
		{
			diffuseMap.freeAllData();
			normalMap.freeAllData();

			geometryPassShader.freeAllData();
			lightsPassShader.freeAllData();
			finalPassShader.freeAllData();

			mdl.freeAllData();
			gbuffer.freeAllData();
		}

		// Loop until the user closes the window:
		while (!glfwWindowShouldClose(glWindow))
		{
			camera.update();

			glViewport(0, 0, launchOptions.framebufferWidth, launchOptions.framebufferHeight);
			glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			gbuffer.bind(GBuffer.Pass.Geometry);
			{
				glViewport(0, 0, launchOptions.windowWidth, launchOptions.windowHeight);
				glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				diffuseMap.bind(0);
				normalMap.bind(1);
				renderScene(geometryPassShader, mdl);
			}

			gbuffer.bind(GBuffer.Pass.Lights);
			{
				glViewport(0, 0, launchOptions.windowWidth, launchOptions.windowHeight);
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT); // No depth buffer for this pass

				glEnable(GL_BLEND);
				glBlendFunc(GL_ONE, GL_ONE);

				glDepthMask(GL_FALSE);

				renderLights(lightsPassShader);

				glDepthMask(GL_TRUE);

				glCullFace(GL_BACK);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			}

			gbuffer.bind(GBuffer.Pass.Final);
			{
			    //glDisable(GL_BLEND);
				glViewport(0, 0, launchOptions.framebufferWidth, launchOptions.framebufferHeight);
				renderer.drawFullscreenQuadrilateral(finalPassShader);
			}

			// CLEANUP:
			glDisable(GL_BLEND);

			gbuffer.visualizeRenderTargets(launchOptions.framebufferWidth, launchOptions.framebufferHeight, 512, 384);
			//gbuffer.visualizeRenderTargets(launchOptions.framebufferWidth, launchOptions.framebufferHeight, 1024, 768);

			glfwSwapBuffers(glWindow);
			glfwPollEvents();

			// Checking for OpenGL errors every frame has a cost,
			// so we only do it when running in debug mode.
			debug
			{
				renderer.checkErrors(/* throwException = */ true);
			}
		}
	}

	void shutdown()
	{
		Common.logComment("Application shutting down...");

		renderer.shutdown();

		if (glWindow !is null)
		{
			glfwDestroyWindow(glWindow);
			glWindow = null;
		}

		glfwTerminate();
	}

	const(LaunchOptions *) getLaunchOptions() const
	{
		return &launchOptions;
	}

	Renderer getRenderer()
	{
		return renderer;
	}

	void onMouseMotion(float x, float y)
	{
		camera.mouseInput(cast(int)x, cast(int)y);
	}

	void onKeyUp(int keyCode, int modifiers)
	{
		camera.keyInput(cast(char)keyCode, false);
	}

	void onKeyDown(int keyCode, int modifiers)
	{
		camera.keyInput(cast(char)keyCode, true);
	}

	private void initGLFW()
	{
		Common.logComment("Initializing GLFW...");

		// Load OpenGL versions 1.0 and 1.1:
		DerelictGL3.load();

		// Load GLFW library:
		DerelictGLFW3.load();

		// Initialize:
		glfwSetErrorCallback(&GLFWErrorCallback);
		if (!glfwInit())
		{
			Common.fatalError("Failed to initialize GLFW! Unable to continue.");
		}

		Common.logComment("GLFW version: ", std.conv.to!string(glfwGetVersionString()));

		// Require Core OpenGL 3.2 or higher:
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

		// Set other window hints:
		glfwWindowHint(GLFW_RESIZABLE,  GL_FALSE);
		glfwWindowHint(GLFW_RED_BITS,   8);
		glfwWindowHint(GLFW_GREEN_BITS, 8);
		glfwWindowHint(GLFW_BLUE_BITS,  8);
		glfwWindowHint(GLFW_ALPHA_BITS, 8);
		glfwWindowHint(GLFW_DEPTH_BITS, 24);

		// Multisampling AA:
		glfwWindowHint(GLFW_SAMPLES, launchOptions.numSamples);

		// Create a window and its OpenGL context:
		glWindow = glfwCreateWindow(launchOptions.windowWidth, launchOptions.windowHeight,
				std.string.toStringz(appTitle), null, null);

		if (!glWindow)
		{
			glfwTerminate();
			Common.fatalError("Failed to create a GLFW window or OpenGL rendering context!");
		}

		// Make the window's GL context current:
		glfwMakeContextCurrent(glWindow);

		// Set V-sync:
		if (launchOptions.vsync)
		{
			glfwSwapInterval(1);
		}
		else
		{
			glfwSwapInterval(0);
		}

		// This will ensure the mouse cursor is visible.
		glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

		// Save the 'this' pointer so we can recover it inside the callbacks:
		glfwSetWindowUserPointer(glWindow, cast(void *)this);

		// Set application event callbacks:
		glfwSetCursorPosCallback(glWindow, &GLFWCursorPosCallback);
		glfwSetKeyCallback(glWindow, &GLFWKeyCallback);

		Common.logComment("GLFW initialized!");
	}

	private void initOpenGL()
	{
		Common.logComment("Initializing OpenGL...");

		// Load versions 1.2+ and all supported ARB and EXT extensions.
		DerelictGL3.reload();

		Common.logComment("GL_VERSION....: ", std.conv.to!string(glGetString(GL_VERSION)));
		Common.logComment("GL_RENDERER...: ", std.conv.to!string(glGetString(GL_RENDERER)));
		Common.logComment("GL_VENDOR.....: ", std.conv.to!string(glGetString(GL_VENDOR)));
		Common.logComment("GLSL_VERSION..: ", std.conv.to!string(glGetString(GL_SHADING_LANGUAGE_VERSION)));

		glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
		glClear(GL_COLOR_BUFFER_BIT);

		int fbWidth = 0, fbHeight = 0;
		glfwGetFramebufferSize(glWindow, &fbWidth, &fbHeight);
		glViewport(0, 0, fbWidth, fbHeight);

		Common.logComment("Framebuffer size: ", fbWidth, "x", fbHeight);
		launchOptions.framebufferWidth  = fbWidth;
		launchOptions.framebufferHeight = fbHeight;

		renderer = new OpenGLRenderer();
		renderer.initWithOptions(launchOptions);

		// Clear possible errors generated by GLFW and others.
		while (glGetError() != GL_NO_ERROR) { }

		Common.logComment("OpenGL initialized!");
	}
}

// ========================================================
// GLFW callbacks / local helpers:
// ========================================================

private string GLFWErrCodeStr(GLenum errCode) nothrow
{
	switch (errCode)
	{
	case GLFW_NOT_INITIALIZED     : return "GLFW_NOT_INITIALIZED";
	case GLFW_NO_CURRENT_CONTEXT  : return "GLFW_NO_CURRENT_CONTEXT";
	case GLFW_INVALID_ENUM        : return "GLFW_INVALID_ENUM";
	case GLFW_INVALID_VALUE       : return "GLFW_INVALID_VALUE";
	case GLFW_OUT_OF_MEMORY       : return "GLFW_OUT_OF_MEMORY";
	case GLFW_API_UNAVAILABLE     : return "GLFW_API_UNAVAILABLE";
	case GLFW_VERSION_UNAVAILABLE : return "GLFW_VERSION_UNAVAILABLE";
	case GLFW_PLATFORM_ERROR      : return "GLFW_PLATFORM_ERROR";
	case GLFW_FORMAT_UNAVAILABLE  : return "GLFW_FORMAT_UNAVAILABLE";
	default                       : return "Unknown GLFW error";
	} // End switch (errCode)
}

extern(C)
{
	private void GLFWErrorCallback(int errCode, const(char) * message) nothrow
	{
		Common.logWarning("GLFW reported error '", GLFWErrCodeStr(errCode), "': ", message);
	}

	private void GLFWCursorPosCallback(GLFWwindow * win, double x, double y) nothrow
	{
		try
		{
			GLFW3Application app = cast(GLFW3Application)glfwGetWindowUserPointer(win);
			assert(app !is null);

			app.onMouseMotion(cast(float)x, cast(float)y);
		}
		catch (Exception) { }
	}

	private void GLFWKeyCallback(GLFWwindow * win, int glfwKeyCode, int sysKeyCode, int action, int mods) nothrow
	{
		try
		{
			GLFW3Application app = cast(GLFW3Application)glfwGetWindowUserPointer(win);
			assert(app !is null);

			switch (action)
			{
			case GLFW_RELEASE :
				app.onKeyUp(glfwKeyCode, mods);
				break;

			case GLFW_PRESS :
				app.onKeyDown(glfwKeyCode, mods);
				break;

			default :
				// GLFW_REPEAT ignored...
				return;
			} // End switch (action)
		}
		catch (Exception) { }
	}
}
