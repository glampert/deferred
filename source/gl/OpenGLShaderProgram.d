
// ================================================================================================
// -*- D -*-
// File: OpenGLShaderProgram.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: OpenGL specific parts of the ShaderProgram interface.
// ================================================================================================

module deferred.gl.OpenGLShaderProgram;

private
{
	import deferred.Common;
	import deferred.MathUtils;
	import deferred.ShaderProgram;
	import deferred.gl.OpenGLRenderer;
	import derelict.opengl3.gl3;
	static import std.array;
	static import std.ascii;
	static import std.file;
	static import std.regex;
	static import std.stdio;
	static import std.string;
}

// ========================================================
// OpenGLShaderProgram:
// ========================================================

class OpenGLShaderProgram : ShaderProgram
{
	private
	{
		GLuint         programId; // OpenGL resource handle.
		OpenGLRenderer renderer;  // Renderer that owns the resource.
	}

	// ----------------------------------------------------

	this(OpenGLRenderer renderer)
	{
		assert(renderer !is null);
		this.renderer = renderer;
	}

	~this()
	{
		freeAllData();
	}

	void loadFromFile(const string vsFile, const string fsFile)
	{
		assert(vsFile !is null);
		assert(fsFile !is null);

		const char[] glslVersion = std.conv.to!(const char[])(renderer.getGLSLVersionTag());

		Common.logComment("Loading Vertex Shader from file '", vsFile, "'...");
		char[] vsSource = glslVersion.dup; // First thing is to append the #version directive.
		vsSource ~= loadFileResolvingIncludes(vsFile);
		vsSource ~= '\0';

		Common.logComment("Loading Fragment Shader from file '", fsFile, "'...");
		char[] fsSource = glslVersion.dup; // First thing is to append the #version directive.
		fsSource ~= loadFileResolvingIncludes(fsFile);
		fsSource ~= '\0';

		initFromData(vsSource, fsSource);
	}

	void initFromData(const char[] vsSource, const char[] fsSource)
	{
		assert(vsSource !is null);
		assert(fsSource !is null);

		// Uncomment this for verbose debugging:
		/*
		Common.logComment("------ Creating Vertex Shader from source: ------");
		Common.logComment(vsSource);
		Common.logComment("------ Creating Fragment Shader from source: ------");
		Common.logComment(fsSource);
		*/

		// Allocate new ids:
		programId = glCreateProgram();
		const GLuint vsId = glCreateShader(GL_VERTEX_SHADER);
		const GLuint fsId = glCreateShader(GL_FRAGMENT_SHADER);

		if (programId <= 0 || vsId <= 0 || fsId <= 0)
		{
			if (glIsProgram(programId))
			{
				glDeleteProgram(programId);
				programId = 0;
			}
			if (glIsShader(vsId))
			{
				glDeleteShader(vsId);
			}
			if (glIsShader(fsId))
			{
				glDeleteShader(fsId);
			}
			throw new Exception("Problems when creating shader program!. Failed to allocate GL ids!");
		}

		// Compile & attach shaders:
		Common.logComment("Compiling shaders and linking program...");

		const GLchar * vssrc = vsSource.ptr;
		glShaderSource(vsId, 1, &vssrc, null);
		glCompileShader(vsId);
		glAttachShader(programId, vsId);

		const GLchar * fssrc = fsSource.ptr;
		glShaderSource(fsId, 1, &fssrc, null);
		glCompileShader(fsId);
		glAttachShader(programId, fsId);

		// Link the shader program:
		glLinkProgram(programId);

		// Print errors/warnings for the just compiled shaders and program:
		printInfoLogs(vsId, fsId);

		// After attached to a program the shader objects can be deleted.
		glDeleteShader(vsId);
		glDeleteShader(fsId);
	}

	void freeAllData()
	{
		if (programId != 0)
		{
			unbind(); // Ensure unbound
			glDeleteProgram(programId);
			programId = 0;
		}
	}

	void bind() const
	{
		// This will go thru a caching layer avoiding unnecessary state changes.
		renderer.bindShaderProgram(this);
	}

	void unbind() const
	{
		renderer.bindNullShaderProgram();
	}

	GLuint getGLProgramId() const
	{
		return programId;
	}

	ShaderUniformHandle getShaderUniformHandle(const string varName) const
	{
		assert(varName !is null);
		const GLint loc = glGetUniformLocation(programId, std.string.toStringz(varName));
		if (loc == -1)
		{
			Common.logWarning("Failed to get location for shader uniform '", varName, "' for program ", programId);
		}
		return loc;
	}

	bool setShaderUniform(in ShaderUniformHandle var, int i) const
	{
		if (var == -1)
		{
			Common.logWarning("setShaderUniform() => invalid uniform handle!");
			return false;
		}
		glUniform1i(var, i);
		return true;
	}

	bool setShaderUniform(in ShaderUniformHandle var, float f) const
	{
		if (var == -1)
		{
			Common.logWarning("setShaderUniform() => invalid uniform handle!");
			return false;
		}
		glUniform1f(var, f);
		return true;
	}

	bool setShaderUniform(in ShaderUniformHandle var, in float[] fv) const
	{
		assert(fv !is null);
		if (var == -1)
		{
			Common.logWarning("setShaderUniform() => invalid uniform handle!");
			return false;
		}

		switch (fv.length)
		{
		case 1 :
			glUniform1f(var, fv[0]);
			break;
		case 2 :
			glUniform2f(var, fv[0], fv[1]);
			break;
		case 3 :
			glUniform3f(var, fv[0], fv[1], fv[2]);
			break;
		case 4 :
			glUniform4f(var, fv[0], fv[1], fv[2], fv[3]);
			break;
		default :
			Common.logError("setShaderUniform(float[]) => array length is invalid!");
			return false;
		} // switch (fv.length)

		return true;
	}

	bool setShaderUniform(in ShaderUniformHandle var, const ref Vec3 v3) const
	{
		if (var == -1)
		{
			Common.logWarning("setShaderUniform() => invalid uniform handle!");
			return false;
		}
		glUniform3f(var, v3.x, v3.y, v3.z);
		return true;
	}

	bool setShaderUniform(in ShaderUniformHandle var, const ref Mat4 m4x4) const
	{
		if (var == -1)
		{
			Common.logWarning("setShaderUniform() => invalid uniform handle!");
			return false;
		}
		glUniformMatrix4fv(var, 1, GL_FALSE, m4x4.m.ptr);
		return true;
	}

	private string addSourcePath(const string filename) const
	{
		// Add a default shader source path if the renderer provides one:
		const string path = renderer.getShaderSourcePath();
		if (path != "")
		{
			return std.string.format("%s/%s", path, filename);
		}
		return filename;
	}

	private const char[] loadFileResolvingIncludes(const string filename) const
	{
		// Load a shader text file and resolve non-recursive #includes.
		// The file can have an arbitrary number of #includes, but the included
		// files cannot themselves include other files.

		const string fixedName = addSourcePath(filename);
		auto file = std.stdio.File(fixedName, "rt");
		const ulong fileLength = file.size();

		if (fileLength == 0 || fileLength == ulong.max)
		{
			throw new std.file.FileException(std.string.format("Empty file or bad size for '%s'. Aborting...", fixedName));
		}

		// RegExp used to resolve the #include directives:
		// "[^"]+" => Match sequence of quoted chars that don't have quotes inside, 1 or more times (+)
		auto rexp = std.regex.ctRegex!(`("[^"]+")(\s+|$)`);

		string line;
		char[] source;

		// Read in every line, replacing any #include statements by
		// the included file contents. We don't support recursive includes.
		while ((line = file.readln()) !is null)
		{
			// Resolve single #include:
			if (std.string.indexOf(line, "#include") != -1)
			{
				auto m = std.regex.match(line, rexp);
				auto c = m.captures;

				assert(std.string.strip(c.pre) == "#include");
				const string includedFileName = std.string.removechars(c.hit, "\"\'\t\n\r ");
				if (includedFileName == "")
				{
					throw new Exception("Missing file name for #include directive!");
				}

				Common.logComment("Trying to resolve shader #inlcude '", addSourcePath(includedFileName), "'...");

				// Load text file:
				const char[] includeSource = std.file.readText(addSourcePath(includedFileName));
				if (includeSource.length > 0)
				{
					source ~= '\n';
					source ~= includeSource;
					source ~= '\n';
				}
			}
			else
			{
				// Append the line as is:
				source ~= line;
			}
		}

		source ~= '\n';
		return source;
	}

	private void printInfoLogs(in GLuint vsId, in GLuint fsId) const
	{
		// Info log is printed with Common.logComment():

		GLsizei charsWritten;
		auto infoLogBuf = std.array.uninitializedArray!(char[])(4096);

		charsWritten = 0;
		infoLogBuf[] = '\0'; // Set every char to null
		glGetProgramInfoLog(programId, cast(GLsizei)(infoLogBuf.length - 1), &charsWritten, infoLogBuf.ptr);
		if (charsWritten > 0)
		{
			Common.logComment("------ PROGRAM INFO LOG ------ \n", infoLogBuf, "\n");
		}

		charsWritten = 0;
		infoLogBuf[] = '\0'; // Set every char to null
		glGetShaderInfoLog(vsId, cast(GLsizei)(infoLogBuf.length - 1), &charsWritten, infoLogBuf.ptr);
		if (charsWritten > 0)
		{
			Common.logComment("------ VS INFO LOG ------ \n", infoLogBuf, "\n");
		}

		charsWritten = 0;
		infoLogBuf[] = '\0'; // Set every char to null
		glGetShaderInfoLog(fsId, cast(GLsizei)(infoLogBuf.length - 1), &charsWritten, infoLogBuf.ptr);
		if (charsWritten > 0)
		{
			Common.logComment("------ FS INFO LOG ------ \n", infoLogBuf, "\n");
		}
	}
}
