
// ================================================================================================
// -*- D -*-
// File: ShaderProgram.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: Shader Program interface.
// ================================================================================================

module deferred.ShaderProgram;

public import deferred.MathUtils;

// Opaque handle to shader uniform variables.
alias int ShaderUniformHandle;

// ========================================================
// ShaderProgram interface:
// ========================================================

interface ShaderProgram
{
	// Resolves #includes and adds proper #version tag for GLSL.
	void loadFromFile(const string vsFile, const string fsFile);

	// This one will not resolve #includes nor add the #version tag!
	void initFromData(const char[] vsSource, const char[] fsSource);

	// Frees all system-side and GPU-side data.
	void freeAllData();

	// Bind/unbind for rendering and variable update.
	void bind() const;
	void unbind() const;

	// Find a shader uniform variable by name.
	// Returns a negative value if the variable can't be found.
	ShaderUniformHandle getShaderUniformHandle(const string varName) const;

	// Update shader uniform variables. Shader Program must be bound first!
	bool setShaderUniform(in ShaderUniformHandle var, int i) const;
	bool setShaderUniform(in ShaderUniformHandle var, float f) const;
	bool setShaderUniform(in ShaderUniformHandle var, in float[] fv) const;
	bool setShaderUniform(in ShaderUniformHandle var, const ref Vec3 v3) const;
	bool setShaderUniform(in ShaderUniformHandle var, const ref Mat4 m4x4) const;
}
