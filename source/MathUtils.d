
// ================================================================================================
// -*- D -*-
// File: MathUtils.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: Math objects and utilities.
// ================================================================================================

module deferred.MathUtils;

public
{
	static import std.math;
}

private
{
	import deferred.Common;
}

// ========================================================
// Miscellaneous:
// ========================================================

//
// Degree <=> radians conversion:
//

float degToRad(float degrees)
{
	return degrees * (std.math.PI / 180.0f);
}

float radToDeg(float radians)
{
	return radians * (180.0f / std.math.PI);
}

// ========================================================
// Vec3 structure:
// ========================================================

//
// Euclidian (x,y,z) 3D vector with basic math operations.
// It will also be common in this project to use a Vec3 to represent
// a 3D point. In such cases this name is context dependent.
//
struct Vec3
{
	float x, y, z;

	// Special vector constants:
	static immutable Vec3 One   = Vec3(1.0f, 1.0f, 1.0f);
	static immutable Vec3 Zero  = Vec3(0.0f, 0.0f, 0.0f);
	static immutable Vec3 UnitX = Vec3(1.0f, 0.0f, 0.0f);
	static immutable Vec3 UnitY = Vec3(0.0f, 1.0f, 0.0f);
	static immutable Vec3 UnitZ = Vec3(0.0f, 0.0f, 1.0f);

	this(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	this(const ref Vec3 other)
	{
		this.x = other.x;
		this.y = other.y;
		this.z = other.z;
	}

	float opIndex(size_t index) const // operator[] (const)
	{
		switch (index)
		{
		case 0  : return x;
		case 1  : return y;
		case 2  : return z;
		default : assert(false);
		}
	}

	float opIndexAssign(float value, size_t index) // operator[] (for assignment)
	{
		switch (index)
		{
		case 0  : return x = value;
		case 1  : return y = value;
		case 2  : return z = value;
		default : assert(false);
		}
	}

	Vec3 opBinary(string op)(const ref Vec3 rhs)
	{
		static if (op == "+")
		{
			return add(this, rhs);
		}
		else static if (op == "-")
		{
			return sub(this, rhs);
		}
		else static if (op == "*")
		{
			return mul(this, rhs);
		}
		else static assert(false, "Operator " ~ op ~ " not implemented!");
	}

	Vec3 opBinary(string op)(float rhs)
	{
		static if (op == "+")
		{
			return add(this, rhs);
		}
		else static if (op == "-")
		{
			return sub(this, rhs);
		}
		else static if (op == "*")
		{
			return mul(this, rhs);
		}
		else static if (op == "/")
		{
			return div(this, rhs);
		}
		else static assert(false, "Operator " ~ op ~ " not implemented!");
	}

	float length() const
	{
		return std.math.sqrt((x * x) + (y * y) + (z * z));
	}

	float invLength() const
	{
		return 1.0f / std.math.sqrt((x * x) + (y * y) + (z * z));
	}

	float squaredLength() const
	{
		return (x * x) + (y * y) + (z * z);
	}

	float dot(const ref Vec3 v) const
	{
		return (x * v.x) + (y * v.y) + (z * v.z);
	}

	Vec3 cross(const ref Vec3 v) const
	{
		Vec3 result;
		result.x = (y * v.z) - (z * v.y);
		result.y = (z * v.x) - (x * v.z);
		result.z = (x * v.y) - (y * v.x);
		return result;
	}

	Vec3 normalized() const // Normalized copy of this
	{
		const float invLen = invLength();
		return Vec3(x * invLen, y * invLen, z * invLen);
	}

	void normalizeSelf()
	{
		const float invLen = invLength();
		x *= invLen;
		y *= invLen;
		z *= invLen;
	}

	float distance(const ref Vec3 v) const
	{
		const Vec3 diff = Vec3.sub(this, v); // a - b
		return diff.length();
	}

	static Vec3 add(const ref Vec3 a, const ref Vec3 b) // Add vectors
	{
		return Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	static Vec3 add(const ref Vec3 v, float s) // Add vector with scalar
	{
		return Vec3(v.x + s, v.y + s, v.z + s);
	}

	static Vec3 sub(const ref Vec3 a, const ref Vec3 b) // Subtract vectors
	{
		return Vec3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	static Vec3 sub(const ref Vec3 v, float s) // Subtract with scalar
	{
		return Vec3(v.x - s, v.y - s, v.z - s);
	}

	static Vec3 mul(const ref Vec3 a, const ref Vec3 b) // Multiply vectors
	{
		return Vec3(a.x * b.x, a.y * b.y, a.z * b.z);
	}

	static Vec3 mul(const ref Vec3 v, float s) // Multiply vector by scalar
	{
		return Vec3(v.x * s, v.y * s, v.z * s);
	}

	static Vec3 div(const ref Vec3 v, float s) // Divide vector by scalar
	{
		return Vec3(v.x / s, v.y / s, v.z / s);
	}
}

// ========================================================
// Mat4 structure:
// ========================================================

//
// Homogeneous 4x4 column-major matrix, for affine 3D transformations.
// Rotations are counterclockwise. Uses a right-handed coordinate system.
//
struct Mat4
{
	//
	// Layout:
	//  | m[0] m[4] m[8]  m[12] |
	//  | m[1] m[5] m[9]  m[13] |
	//  | m[2] m[6] m[10] m[14] |
	//  | m[3] m[7] m[11] m[15] |
	//
	float m[16]; // Matrix elements (column major - OpenGL style)

	// Construct explicit matrix with all elements:
	this(float m0, float m4, float  m8, float m12,
	     float m1, float m5, float  m9, float m13,
	     float m2, float m6, float m10, float m14,
	     float m3, float m7, float m11, float m15)
	{
		m[0] = m0; m[4] = m4; m[8]  = m8;  m[12] = m12;
		m[1] = m1; m[5] = m5; m[9]  = m9;  m[13] = m13;
		m[2] = m2; m[6] = m6; m[10] = m10; m[14] = m14;
		m[3] = m3; m[7] = m7; m[11] = m11; m[15] = m15;
	}

	// Set every element of the matrix to zero.
	void setZero()
	{
		m[0] = 0.0f; m[4] = 0.0f; m[8]  = 0.0f; m[12] = 0.0f;
		m[1] = 0.0f; m[5] = 0.0f; m[9]  = 0.0f; m[13] = 0.0f;
		m[2] = 0.0f; m[6] = 0.0f; m[10] = 0.0f; m[14] = 0.0f;
		m[3] = 0.0f; m[7] = 0.0f; m[11] = 0.0f; m[15] = 0.0f;
	}

	// Set the matrix to an identity matrix.
	void setIdentity()
	{
		m[0] = 1.0f; m[4] = 0.0f; m[8]  = 0.0f; m[12] = 0.0f;
		m[1] = 0.0f; m[5] = 1.0f; m[9]  = 0.0f; m[13] = 0.0f;
		m[2] = 0.0f; m[6] = 0.0f; m[10] = 1.0f; m[14] = 0.0f;
		m[3] = 0.0f; m[7] = 0.0f; m[11] = 0.0f; m[15] = 1.0f;
	}

	// Builds an orthographic matrix.
	void setOrtho(float left, float right, float botton, float top, float zNear, float zFar)
	{
		const Vec3 v1 = Vec3(right, top,    zFar);
		const Vec3 v2 = Vec3(left,  botton, zNear);

		const Vec3 delta = Vec3.sub(v1, v2);
		const Vec3 sum   = Vec3.add(v1, v2);

		const Vec3 ratio    = Vec3(sum.x / delta.x, sum.y / delta.y, sum.z / delta.z);
		const Vec3 twoRatio = Vec3(2.0f / delta.x, 2.0f / delta.y, 2.0f / delta.z);

		m[0] = twoRatio.x; m[4] = 0.0f;       m[8 ] = 0.0f;        m[12] = -ratio.x;
		m[1] = 0.0f;       m[5] = twoRatio.y; m[9 ] = 0.0f;        m[13] = -ratio.y;
		m[2] = 0.0f;       m[6] = 0.0f;       m[10] = -twoRatio.z; m[14] = -ratio.z;
		m[3] = 0.0f;       m[7] = 0.0f;       m[11] = 0.0f;        m[15] = 1.0f;
	}

	// Builds a frustum matrix (A perspective projection matrix).
	void setFrustum(float left, float right, float botton, float top, float zNear, float zFar)
	{
		const float dz = (2.0f * zNear);

		const Vec3 v1 = Vec3(right, top,    zFar);
		const Vec3 v2 = Vec3(left,  botton, zNear);

		const Vec3 delta = Vec3.sub(v1, v2);
		const Vec3 sum   = Vec3.add(v1, v2);

		const Vec3 ratio    = Vec3(sum.x / delta.x, sum.y / delta.y, sum.z / delta.z);
		const Vec3 twoRatio = Vec3(dz / delta.x, dz / delta.y, dz / delta.z);

		m[0] = twoRatio.x; m[4] = 0.0f;       m[8 ] =  ratio.x; m[12] = 0.0f;
		m[1] = 0.0f;       m[5] = twoRatio.y; m[9 ] =  ratio.y; m[13] = 0.0f;
		m[2] = 0.0f;       m[6] = 0.0f;       m[10] = -ratio.z; m[14] = -(zFar * twoRatio.z);
		m[3] = 0.0f;       m[7] = 0.0f;       m[11] = -1.0f;    m[15] = 0.0f;
	}

	// Builds a right-handed look-at matrix.
	void setLookAt(const ref Vec3 eye, const ref Vec3 lookAt, const ref Vec3 up)
	{
		Vec3 zAxis = Vec3.sub(eye, lookAt); // Right-handed
		zAxis.normalizeSelf();

		Vec3 xAxis = up.cross(zAxis);
		xAxis.normalizeSelf();

		Vec3 yAxis = zAxis.cross(xAxis);

		m[0] = xAxis.x; m[4] = xAxis.y; m[8 ] = xAxis.z; m[12] = -xAxis.dot(eye);
		m[1] = yAxis.x; m[5] = yAxis.y; m[9 ] = yAxis.z; m[13] = -yAxis.dot(eye);
		m[2] = zAxis.x; m[6] = zAxis.y; m[10] = zAxis.z; m[14] = -zAxis.dot(eye);
		m[3] = 0.0f;    m[7] = 0.0f;    m[11] = 0.0f;    m[15] = 1.0f;
	}

	// Builds a perspective projection matrix with field of view.
	void setPerspectiveFov(float fovy, float aspect, float zNear, float zFar)
	{
		immutable float TwoPIOver360 = ((2.0f * std.math.PI) / 360.0f);
		const float halfHeight = zNear * std.math.tan(fovy * 0.5f * TwoPIOver360);
		const float halfWidth  = halfHeight * aspect;
		setFrustum(-halfWidth, halfWidth, -halfHeight, halfHeight, zNear, zFar);
	}

	void setTranslation(float tx, float ty, float tz)
	{
		setIdentity();
		m[12] = tx;
		m[13] = ty;
		m[14] = tz;
	}

	void setTranslation(const ref Vec3 trans)
	{
		setIdentity();
		m[12] = trans.x;
		m[13] = trans.y;
		m[14] = trans.z;
	}

	void setTranslationX(float dist)
	{
		setIdentity();
		m[12] = dist;
	}

	void setTranslationY(float dist)
	{
		setIdentity();
		m[13] = dist;
	}

	void setTranslationZ(float dist)
	{
		setIdentity();
		m[14] = dist;
	}

	void setRotation(const ref Vec3 axis, float radians) // Axis assumed to be normalized!
	{
		const float s = std.math.sin(radians);
		const float c = std.math.cos(radians);
		const float oneMinusCos = 1.0f - c;

		const float x2 = axis.x * axis.x;
		const float y2 = axis.y * axis.y;
		const float z2 = axis.z * axis.z;

		const float xym = axis.x * axis.y * oneMinusCos;
		const float xzm = axis.x * axis.z * oneMinusCos;
		const float yzm = axis.y * axis.z * oneMinusCos;

		const float xSin = axis.x * s;
		const float ySin = axis.y * s;
		const float zSin = axis.z * s;

		m[ 0] = x2 * oneMinusCos + c;
		m[ 1] = xym + zSin;
		m[ 2] = xzm - ySin;
		m[ 3] = 0.0f;
		m[ 4] = xym - zSin;
		m[ 5] = y2 * oneMinusCos + c;
		m[ 6] = yzm + xSin;
		m[ 7] = 0.0f;

		m[ 8] = xzm + ySin;
		m[ 9] = yzm - xSin;
		m[10] = z2 * oneMinusCos + c;
		m[11] = 0.0f;

		m[12] = 0.0f;
		m[13] = 0.0f;
		m[14] = 0.0f;
		m[15] = 1.0f;
	}

	void setRotationX(float radians)
	{
		const float s = std.math.sin(radians);
		const float c = std.math.cos(radians);
		setIdentity();
		m[5]  =  c;
		m[6]  =  s;
		m[9]  = -s;
		m[10] =  c;
	}

	void setRotationY(float radians)
	{
		const float s = std.math.sin(radians);
		const float c = std.math.cos(radians);
		setIdentity();
		m[0]  =  c;
		m[2]  = -s;
		m[8]  =  s;
		m[10] =  c;
	}

	void setRotationZ(float radians)
	{
		const float s = std.math.sin(radians);
		const float c = std.math.cos(radians);
		setIdentity();
		m[0] =  c;
		m[1] =  s;
		m[4] = -s;
		m[5] =  c;
	}

	void setScale(float sx, float sy, float sz)
	{
		setZero();
		m[0]  = sx;
		m[5]  = sy;
		m[10] = sz;
		m[15] = 1.0f;
	}

	void setScale(const ref Vec3 scale)
	{
		setZero();
		m[0]  = scale.x;
		m[5]  = scale.y;
		m[10] = scale.z;
		m[15] = 1.0f;
	}

	void setScaleX(float amt)
	{
		setZero();
		m[0]  = amt;
		m[5]  = 1.0f;
		m[10] = 1.0f;
		m[15] = 1.0f;
	}

	void setScaleY(float amt)
	{
		setZero();
		m[0]  = 1.0f;
		m[5]  = amt;
		m[10] = 1.0f;
		m[15] = 1.0f;
	}

	void setScaleZ(float amt)
	{
		setZero();
		m[0]  = 1.0f;
		m[5]  = 1.0f;
		m[10] = amt;
		m[15] = 1.0f;
	}

	bool inverseSelf()
	{
		const float a0  = (m[ 0] * m[ 5]) - (m[ 1] * m[ 4]);
		const float a1  = (m[ 0] * m[ 6]) - (m[ 2] * m[ 4]);
		const float a2  = (m[ 0] * m[ 7]) - (m[ 3] * m[ 4]);
		const float a3  = (m[ 1] * m[ 6]) - (m[ 2] * m[ 5]);
		const float a4  = (m[ 1] * m[ 7]) - (m[ 3] * m[ 5]);
		const float a5  = (m[ 2] * m[ 7]) - (m[ 3] * m[ 6]);
		const float b0  = (m[ 8] * m[13]) - (m[ 9] * m[12]);
		const float b1  = (m[ 8] * m[14]) - (m[10] * m[12]);
		const float b2  = (m[ 8] * m[15]) - (m[11] * m[12]);
		const float b3  = (m[ 9] * m[14]) - (m[10] * m[13]);
		const float b4  = (m[ 9] * m[15]) - (m[11] * m[13]);
		const float b5  = (m[10] * m[15]) - (m[11] * m[14]);
		const float det = (a0 * b5) - (a1 * b4) + (a2 * b3) + (a3 * b2) - (a4 * b1) + (a5 * b0);

		if (std.math.fabs(det) > 0.0f)
		{
			Mat4 inverse;
			inverse.m[ 0] = + (m[ 5] * b5) - (m[ 6] * b4) + (m[ 7] * b3);
			inverse.m[ 4] = - (m[ 4] * b5) + (m[ 6] * b2) - (m[ 7] * b1);
			inverse.m[ 8] = + (m[ 4] * b4) - (m[ 5] * b2) + (m[ 7] * b0);
			inverse.m[12] = - (m[ 4] * b3) + (m[ 5] * b1) - (m[ 6] * b0);
			inverse.m[ 1] = - (m[ 1] * b5) + (m[ 2] * b4) - (m[ 3] * b3);
			inverse.m[ 5] = + (m[ 0] * b5) - (m[ 2] * b2) + (m[ 3] * b1);
			inverse.m[ 9] = - (m[ 0] * b4) + (m[ 1] * b2) - (m[ 3] * b0);
			inverse.m[13] = + (m[ 0] * b3) - (m[ 1] * b1) + (m[ 2] * b0);
			inverse.m[ 2] = + (m[13] * a5) - (m[14] * a4) + (m[15] * a3);
			inverse.m[ 6] = - (m[12] * a5) + (m[14] * a2) - (m[15] * a1);
			inverse.m[10] = + (m[12] * a4) - (m[13] * a2) + (m[15] * a0);
			inverse.m[14] = - (m[12] * a3) + (m[13] * a1) - (m[14] * a0);
			inverse.m[ 3] = - (m[ 9] * a5) + (m[10] * a4) - (m[11] * a3);
			inverse.m[ 7] = + (m[ 8] * a5) - (m[10] * a2) + (m[11] * a1);
			inverse.m[11] = - (m[ 8] * a4) + (m[ 9] * a2) - (m[11] * a0);
			inverse.m[15] = + (m[ 8] * a3) - (m[ 9] * a1) + (m[10] * a0);

			const float invDet = 1.0f / det;
			inverse.m[ 0] *= invDet;
			inverse.m[ 1] *= invDet;
			inverse.m[ 2] *= invDet;
			inverse.m[ 3] *= invDet;
			inverse.m[ 4] *= invDet;
			inverse.m[ 5] *= invDet;
			inverse.m[ 6] *= invDet;
			inverse.m[ 7] *= invDet;
			inverse.m[ 8] *= invDet;
			inverse.m[ 9] *= invDet;
			inverse.m[10] *= invDet;
			inverse.m[11] *= invDet;
			inverse.m[12] *= invDet;
			inverse.m[13] *= invDet;
			inverse.m[14] *= invDet;
			inverse.m[15] *= invDet;

			// Assign new matrix:
			this.m[0] = inverse.m[0]; this.m[4] = inverse.m[4]; this.m[8]  = inverse.m[8];  this.m[12] = inverse.m[12];
			this.m[1] = inverse.m[1]; this.m[5] = inverse.m[5]; this.m[9]  = inverse.m[9];  this.m[13] = inverse.m[13];
			this.m[2] = inverse.m[2]; this.m[6] = inverse.m[6]; this.m[10] = inverse.m[10]; this.m[14] = inverse.m[14];
			this.m[3] = inverse.m[3]; this.m[7] = inverse.m[7]; this.m[11] = inverse.m[11]; this.m[15] = inverse.m[15];
			return true;
		}

		return false; // Cannot be inversed!
	}

	// Transforms point vector (x, y, z, 1) by this matrix.
	Vec3 transformPoint(const ref Vec3 v) const
	{
		const float vx = v.x * m[0] + v.y * m[4] + v.z * m[8]  + m[12];
		const float vy = v.x * m[1] + v.y * m[5] + v.z * m[9]  + m[13];
		const float vz = v.x * m[2] + v.y * m[6] + v.z * m[10] + m[14];
		return Vec3(vx, vy, vz);
	}

	// Multiplies matrix a and b returning result c.
	static Mat4 mul(const ref Mat4 a, const ref Mat4 b)
	{
		Mat4 result;

		result.m[0]  = (a.m[0] * b.m[0]) + (a.m[4] * b.m[1]) + (a.m[8]  * b.m[2]) + (a.m[12] * b.m[3]);
		result.m[1]  = (a.m[1] * b.m[0]) + (a.m[5] * b.m[1]) + (a.m[9]  * b.m[2]) + (a.m[13] * b.m[3]);
		result.m[2]  = (a.m[2] * b.m[0]) + (a.m[6] * b.m[1]) + (a.m[10] * b.m[2]) + (a.m[14] * b.m[3]);
		result.m[3]  = (a.m[3] * b.m[0]) + (a.m[7] * b.m[1]) + (a.m[11] * b.m[2]) + (a.m[15] * b.m[3]);

		result.m[4]  = (a.m[0] * b.m[4]) + (a.m[4] * b.m[5]) + (a.m[8]  * b.m[6]) + (a.m[12] * b.m[7]);
		result.m[5]  = (a.m[1] * b.m[4]) + (a.m[5] * b.m[5]) + (a.m[9]  * b.m[6]) + (a.m[13] * b.m[7]);
		result.m[6]  = (a.m[2] * b.m[4]) + (a.m[6] * b.m[5]) + (a.m[10] * b.m[6]) + (a.m[14] * b.m[7]);
	    result.m[7]  = (a.m[3] * b.m[4]) + (a.m[7] * b.m[5]) + (a.m[11] * b.m[6]) + (a.m[15] * b.m[7]);

		result.m[8]  = (a.m[0] * b.m[8]) + (a.m[4] * b.m[9]) + (a.m[8]  * b.m[10]) + (a.m[12] * b.m[11]);
		result.m[9]  = (a.m[1] * b.m[8]) + (a.m[5] * b.m[9]) + (a.m[9]  * b.m[10]) + (a.m[13] * b.m[11]);
		result.m[10] = (a.m[2] * b.m[8]) + (a.m[6] * b.m[9]) + (a.m[10] * b.m[10]) + (a.m[14] * b.m[11]);
		result.m[11] = (a.m[3] * b.m[8]) + (a.m[7] * b.m[9]) + (a.m[11] * b.m[10]) + (a.m[15] * b.m[11]);

		result.m[12] = (a.m[0] * b.m[12]) + (a.m[4] * b.m[13]) + (a.m[8]  * b.m[14]) + (a.m[12] * b.m[15]);
		result.m[13] = (a.m[1] * b.m[12]) + (a.m[5] * b.m[13]) + (a.m[9]  * b.m[14]) + (a.m[13] * b.m[15]);
		result.m[14] = (a.m[2] * b.m[12]) + (a.m[6] * b.m[13]) + (a.m[10] * b.m[14]) + (a.m[14] * b.m[15]);
		result.m[15] = (a.m[3] * b.m[12]) + (a.m[7] * b.m[13]) + (a.m[11] * b.m[14]) + (a.m[15] * b.m[15]);

		return result;
	}

	// Convert from world (global) coordinates to local model coordinates.
	// Input matrix must be the inverse of the model matrix.
	static Vec3 worldPointToObject(const ref Mat4 invModelMatrix, const ref Vec3 point)
	{
		return invModelMatrix.transformPoint(point);
	}

	// Convert from object (model) space to global world coordinates.
	// Input matrix is the model matrix.
	static Vec3 objectPointToWorld(const ref Mat4 modelMatrix, const ref Vec3 point)
	{
		return modelMatrix.transformPoint(point);
	}
}
