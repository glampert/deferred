
// ================================================================================================
// -*- D -*-
// File: Common.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-06
// Brief: Common utilities.
// ================================================================================================

module deferred.Common;

private
{
	static import std.c.process;
	static import std.stdio;
	static import std.string;
}

// ========================================================
// Common utilities singleton:
// ========================================================

class Common
{
	private
	{
		static bool printComments = true;
		static bool printWarnings = true;
		static bool printErrors   = true;
	}

	// ----------------------------------------------------

	// Set log verbosity levels.
	static void setLogVerbosity(bool printComments, bool printWarnings, bool printErrors) nothrow
	{
		Common.printComments = printComments;
		Common.printWarnings = printWarnings;
		Common.printErrors   = printErrors;
	}

	// Log a comment/verbose message.
	static void logComment(T ...)(T args) nothrow
	{
		try
		{
			if (printComments)
			{
				std.stdio.writeln(args);
			}
		}
		catch (Exception) { }
	}

	// Log warning message.
	static void logWarning(T ...)(T args) nothrow
	{
		try
		{
			if (printWarnings)
			{
				std.stdio.writeln("WARNING: ", args);
			}
		}
		catch (Exception) { }
	}

	// Log error message.
	static void logError(T ...)(T args) nothrow
	{
		try
		{
			if (printErrors)
			{
				std.stdio.writeln("ERROR: ", args);
			}
		}
		catch (Exception) { }
	}

	// Terminate application with a fatal error message.
	static void fatalError(T ...)(T args)
	{
		// Fatal errors are always printed:
		std.stdio.writeln("FATAL ERROR: ", args);
		std.c.process.abort();
	}

	// Returns the first line of a string (first sequence
	// of chars before a '\n'), not including the \n.
	static string oneliner(const string text)
	{
		const ptrdiff_t n = std.string.indexOf(text, '\n');
		if (n == -1)
		{
			return text;
		}
		return text[0 .. n];
	}

	// Disable external instantiation (singleton).
	private this() { }
}
