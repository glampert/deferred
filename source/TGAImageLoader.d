
// ================================================================================================
// -*- D -*-
// File: TgaImageLoader.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-24
// Brief: Loads TrueVision TGA image files.
// ================================================================================================

module deferred.TgaImageLoader;

private
{
	import deferred.Common;
	import deferred.ImageLoader;
	import deferred.Texture;
	import core.stdc.string;
	static import std.array;
	static import std.file;
}

// ========================================================
// TgaImageLoader:
// ========================================================

class TgaImageLoader : ImageLoader
{
	private
	{
		// TrueVision TGA header:
		struct TgaHeader
		{
		align(1):
			ubyte  idLenght;        // Size of image id
			ubyte  hasColormap;     // 1 it has a colormap
			ubyte  imageType;       // Compression type
			ushort cmFirstEntry;    // Colormap origin
			ushort cmLength;        // Colormap length
			ubyte  cmSize;          // Colormap size
			ushort xOrigin;         // Bottom left x coord origin
			ushort yOrigin;         // Bottom left y coord origin
			ushort width;           // Picture width (in pixels)
			ushort height;          // Picture height (in pixels)
			ubyte  pixelDepth;      // Bits per pixel: 8, 16, 24 or 32
			ubyte  imageDescriptor; // 24 bits = 0x00; 32 bits = 0x80
		}

		// Make sure alignment/sizes are correct:
		static assert(ubyte.sizeof  == 1);
		static assert(ushort.sizeof == 2);
		static assert(TgaHeader.sizeof == (ubyte.sizeof * 6) + (ushort.sizeof * 6));

		// If true, forces BGR[A] images to RGB[A].
		bool forceRGB = false;

		// Pixel formats. Set according the 'forceRGB' flag.
		Texture.PixelFormat tgaPixFormat3 = Texture.PixelFormat.BGR_U8;
		Texture.PixelFormat tgaPixFormat4 = Texture.PixelFormat.BGRA_U8;

		// RGBA/BGRA component access tables.
		// Useful for switching from BGRA to RGBA at load time.
		static immutable uint[] colorSwizzle_NOOP    = [ 0, 1, 2, 3 ]; // No-op, keep color as it is
		static immutable uint[] colorSwizzle_BGR2RGB = [ 2, 1, 0, 3 ]; // BGRA to RGBA: 2, 1, 0, 3
	}

	// ----------------------------------------------------

	void loadImageFromFile(const string filename, ref ImageData image)
	{
		assert(filename !is null);
		assert(filename.length != 0);

		Common.logComment("Loading TGA image from file \"", filename, "\"...");
		initImageData(image);

		auto memory = cast(ubyte[])std.file.read(filename);
		if (memory.length == 0)
		{
			throw new Exception("TGA file was empty!");
		}

		loadTgaFromMemory(memory, image);
	}

	void loadImageFromMemory(const ubyte[] memory, ref ImageData image)
	{
		assert(memory !is null);

		Common.logComment("Loading TGA image from memory buffer...");
		initImageData(image);

		if (memory.length == 0)
		{
			throw new Exception("Empty memory buffer!");
		}

		loadTgaFromMemory(memory, image);
	}

	bool loaderSetPropertyInt(const string propName, int value)
	{
		assert(propName !is null);
		if (propName == "forceRGB")
		{
			if (value > 0) // Do force RGB[A]
			{
				forceRGB      = true;
				tgaPixFormat3 = Texture.PixelFormat.RGB_U8;
				tgaPixFormat4 = Texture.PixelFormat.RGBA_U8;
			}
			else // Don't force RGB[A]
			{
				forceRGB      = false;
				tgaPixFormat3 = Texture.PixelFormat.BGR_U8;
				tgaPixFormat4 = Texture.PixelFormat.BGRA_U8;
			}
			return true;
		}
		return false; // Unknown property.
	}

	bool loaderGetPropertyInt(const string propName, ref int value) const
	{
		assert(propName !is null);
		if (propName == "forceRGB")
		{
			value = cast(int)forceRGB;
			return true;
		}
		value = 0;
		return false; // Unknown property.
	}

	private immutable(uint *) getColorSwizzleTable() const nothrow
	{
		if (forceRGB)
		{
			return colorSwizzle_BGR2RGB.ptr;
		}
		return colorSwizzle_NOOP.ptr;
	}

	private static void initImageData(ref ImageData image) nothrow
	{
		image.width  = 0;
		image.height = 0;
		image.numColorComponents = 0;
		image.pixelFormat = Texture.PixelFormat.Invalid;
		image.pixels.clear();
	}

	private static immutable(TgaHeader) readHeader(const(ubyte *) dataPtr) nothrow
	{
		TgaHeader header;
		memcpy(&header, dataPtr, TgaHeader.sizeof);
		return header;
	}

	private void loadTgaFromMemory(const ubyte[] memory, ref ImageData image) const
	{
		if (memory.length < TgaHeader.sizeof)
		{
			throw new Exception("TGA image data packet is smaller than the file header!");
		}

		// I use raw C-style pointers here because this code is
		// actually adapted from and old C++ library I wrote a while back.
		// I'm purposely being lazy :P
		const(ubyte *) dataPtr = memory.ptr;
		ulong position = 0;

		// Read header:
		immutable TgaHeader header = readHeader(dataPtr);
		position += TgaHeader.sizeof;

		// Uncomment this for verbose debugging:
		/*
		Common.logComment("idLenght.........: ", header.idLenght);
		Common.logComment("hasColormap......: ", header.hasColormap);
		Common.logComment("imageType........: ", header.imageType);
		Common.logComment("cmFirstEntry.....: ", header.cmFirstEntry);
		Common.logComment("cmLength.........: ", header.cmLength);
		Common.logComment("cmSize...........: ", header.cmSize);
		Common.logComment("xOrigin..........: ", header.xOrigin);
		Common.logComment("yOrigin..........: ", header.yOrigin);
		Common.logComment("width............: ", header.width);
		Common.logComment("height...........: ", header.height);
		Common.logComment("pixelDepth.......: ", header.pixelDepth);
		Common.logComment("imageDescriptor..: ", header.imageDescriptor);
		*/

		// Validate dimensions:
		if (header.width == 0 || header.height == 0)
		{
			throw new Exception("TGA image header had a zero width/height field!");
		}

		// Skip this if present:
		if (header.idLenght > 0)
		{
			position += header.idLenght;
		}

		// Grayscales are converted to RGB.
		bool needGrayscaleConversion = false;

		// Get image information:
		switch (header.imageType)
		{
		case 0 : // Error! No data
			throw new Exception("No data in TGA image...");

		case 3 :  // Grayscale 8 bits or
		case 11 : // Grayscale 8 bits (RLE)
			image.numColorComponents = 1;
			needGrayscaleConversion  = true;
			break;

		case 1 :  // 8 bits color index
		case 2 :  // BGR 16-24-32 bits
		case 9 :  // 8 bits color index (RLE)
		case 10 : // BGR 16-24-32 bits (RLE)
			if (header.pixelDepth <= 24) // 8 bits and 16 bits images will be converted to 24 bits
			{
				image.numColorComponents = 3;
				image.pixelFormat = tgaPixFormat3;
			}
			else // 32 bits
			{
				image.numColorComponents = 4;
				image.pixelFormat = tgaPixFormat4;
			}
			break;

		default:
			// Image type is not correct, exit
			throw new Exception(std.string.format("Unknown TGA image type %d!", cast(int)header.imageType));
		} // End switch (header.imageType)

		// Memory allocation for pixel data:
		image.width  = header.width;
		image.height = header.height;
		image.pixels.length = (image.width * image.height * image.numColorComponents);

		// Read color map, if present:
		const(ubyte *) rawData  = (header.hasColormap ? (dataPtr + position + (header.cmLength * (header.cmSize >> 3))) : (dataPtr + position));
		const(ubyte *) colormap = (header.hasColormap ? (dataPtr + position) : null); // NOTE: color map is stored in BGR

		// Decode image data:
		switch (header.imageType)
		{
		case 1 :
			// Uncompressed 8 bits color index:
			readTga8bits(image.width, image.height, image.pixels, rawData, colormap);
			break;

		case 2 :
			// Uncompressed 16-24-32 bits:
			switch (header.pixelDepth)
			{
			case 16 :
				readTga16bits(image.width, image.height, image.pixels, rawData);
				break;

			case 24 :
				readTga24bits(image.width, image.height, image.pixels, rawData);
				break;

			case 32 :
				readTga32bits(image.width, image.height, image.pixels, rawData);
				break;

			default :
				throw new Exception(std.string.format("Bad TGA header! pixelDepth = %d", cast(int)header.pixelDepth));
			} // End switch (header.pixelDepth)
			break;

		case 3 :
			// Uncompressed 8 or 16 bits grayscale:
			if (header.pixelDepth == 8)
			{
				readTgaGray8bits(image.width, image.height, image.pixels, rawData);
			}
			else // 16 bits
			{
				readTgaGray16bits(image.width, image.height, image.pixels, rawData);
			}
			break;

		case 9 :
			// RLE compressed 8 bits color index:
			readTga8bitsRLE(image.width, image.height, image.pixels, rawData, colormap);
			break;

		case 10 :
			// RLE compressed 16-24-32 bits:
			switch (header.pixelDepth)
			{
			case 16 :
				readTga16bitsRLE(image.width, image.height, image.pixels, rawData);
				break;

			case 24 :
				readTga24bitsRLE(image.width, image.height, image.pixels, rawData);
				break;

			case 32 :
				readTga32bitsRLE(image.width, image.height, image.pixels, rawData);
				break;

			default :
				throw new Exception(std.string.format("Bad TGA header! pixelDepth = %d", cast(int)header.pixelDepth));
			} // End switch (header.pixelDepth)
			break;

		case 11 :
			// RLE compressed 8 or 16 bits grayscale:
			if (header.pixelDepth == 8)
			{
				readTgaGray8bitsRLE(image.width, image.height, image.pixels, rawData);
			}
			else // 16 bits
			{
				readTgaGray16bitsRLE(image.width, image.height, image.pixels, rawData);
			}
			break;

		default :
			throw new Exception(std.string.format("Bad TGA header! imageType = %d", cast(int)header.imageType));
		} // End switch (header.imageType)

		if (needGrayscaleConversion)
		{
			image.pixels = grayscaleToRGB(image.width, image.height, image.pixels);
			image.pixelFormat = Texture.PixelFormat.RGB_U8;
			image.numColorComponents = 3; // Now RGB
		}

		Common.logComment("TGA loader completed successfully! Imported image is ",
				image.width, "x", image.height, " ", image.pixelFormat);
	}

	private static ubyte[] grayscaleToRGB(const uint width, const uint height, const ubyte[] grayscalePixels)
	{
		const uint pixelCount = (width * height);

		assert(grayscalePixels !is null);
		assert(grayscalePixels.length == pixelCount);

		ubyte[] rgbPixels = new ubyte[pixelCount * 3];
		for (uint i = 0; i < pixelCount; ++i)
		{
			const ubyte color = grayscalePixels[i];

			// Propagate to all three components:
			rgbPixels[(i * 3) + 0] = color;
			rgbPixels[(i * 3) + 1] = color;
			rgbPixels[(i * 3) + 2] = color;
		}
		return rgbPixels;
	}

	private void readTga8bits(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data, const(ubyte *) colormap) const
	{
		assert(data     !is null);
		assert(colormap !is null);

		const uint pixelCount = (width * height);
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		for (uint i = 0; i < pixelCount; ++i)
		{
			// Read index color byte
			const ubyte color = data[i];

			// Convert to RGB 24 bits
			pixels[(i * 3) + colorSwizzleTable[0]] = colormap[(color * 3) + 0];
			pixels[(i * 3) + colorSwizzleTable[1]] = colormap[(color * 3) + 1];
			pixels[(i * 3) + colorSwizzleTable[2]] = colormap[(color * 3) + 2];
		}
	}

	private void readTga16bits(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		const uint pixelCount = (width * height);
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		for (uint i = 0, b = 0; i < pixelCount; ++i, b += 2)
		{
			// Read color word
			const(ubyte *) pData = data + b;
			const ushort color = pData[0] + (pData[1] << 8);

			// Convert to RGB 24 bits
			pixels[(i * 3) + colorSwizzleTable[2]] = (((color & 0x7C00) >> 10) << 3);
			pixels[(i * 3) + colorSwizzleTable[1]] = (((color & 0x03E0) >>  5) << 3);
			pixels[(i * 3) + colorSwizzleTable[0]] = (((color & 0x001F) >>  0) << 3);
		}
	}

	private void readTga24bits(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		const uint pixelCount = (width * height);
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		for (uint i = 0, j = 0; i < pixelCount; ++i)
		{
			// Read RGB 24 bits pixel
			pixels[(i * 3) + colorSwizzleTable[0]] = data[j++];
			pixels[(i * 3) + colorSwizzleTable[1]] = data[j++];
			pixels[(i * 3) + colorSwizzleTable[2]] = data[j++];
		}
	}

	private void readTga32bits(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		const uint pixelCount = (width * height);
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		for (uint i = 0, j = 0; i < pixelCount; ++i)
		{
			// Read RGBA 32 bits pixel
			pixels[(i * 4) + colorSwizzleTable[0]] = data[j++];
			pixels[(i * 4) + colorSwizzleTable[1]] = data[j++];
			pixels[(i * 4) + colorSwizzleTable[2]] = data[j++];
			pixels[(i * 4) + colorSwizzleTable[3]] = data[j++];
		}
	}

	private void readTgaGray8bits(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);
		assert(pixels.length == (width * height));

		memcpy(pixels.ptr, data, (width * height));
	}

	private void readTgaGray16bits(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		// Convert from luminance-alpha to no-alpha grayscale:
		const uint pixelCount = (width * height);
		for (uint i = 0, j = 0; i < pixelCount; ++i)
		{
			pixels[i] = data[j];
			j += 2; // Skip the alpha byte
		}
	}

	private void readTga8bitsRLE(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data, const(ubyte *) colormap) const
	{
		assert(data     !is null);
		assert(colormap !is null);

		uint b = 0;
		ubyte * ptr = pixels.ptr;
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		while (ptr < pixels.ptr + (width * height) * 3)
		{
			// Read first byte
			const ubyte packetHeader = data[b++];
			const uint  size = 1 + (packetHeader & 0x7F);

			if (packetHeader & 0x80)
			{
				// Run-length packet
				const ubyte color = data[b++];
				for (uint i = 0; i < size; ++i, ptr += 3)
				{
					ptr[0] = colormap[(color * 3) + colorSwizzleTable[0]];
					ptr[1] = colormap[(color * 3) + colorSwizzleTable[1]];
					ptr[2] = colormap[(color * 3) + colorSwizzleTable[2]];
				}
			}
			else
			{
				// Non run-length packet
				for (uint i = 0; i < size; ++i, ptr += 3)
				{
					const ubyte color = data[b++];
					ptr[0] = colormap[(color * 3) + colorSwizzleTable[0]];
					ptr[1] = colormap[(color * 3) + colorSwizzleTable[1]];
					ptr[2] = colormap[(color * 3) + colorSwizzleTable[2]];
				}
			}
		}
	}

	private void readTga16bitsRLE(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		uint b = 0;
		ubyte * ptr = pixels.ptr;
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		while (ptr < pixels.ptr + (width * height) * 3)
		{
			// Read first byte
			const ubyte packetHeader = data[b++];
			const uint  size = 1 + (packetHeader & 0x7F);

			if (packetHeader & 0x80)
			{
				// Run-length packet
				const(ubyte *) pData = data + b;
				const ushort color = pData[0] + (pData[1] << 8);
				b += 2;

				for (uint i = 0; i < size; ++i, ptr += 3)
				{
					ptr[colorSwizzleTable[2]] = (((color & 0x7C00) >> 10) << 3);
					ptr[colorSwizzleTable[1]] = (((color & 0x03E0) >>  5) << 3);
					ptr[colorSwizzleTable[0]] = (((color & 0x001F) >>  0) << 3);
				}
			}
			else
			{
				// Non run-length packet
				for (uint i = 0; i < size; ++i, ptr += 3)
				{
					const(ubyte *) pData = data + b;
					const ushort color = pData[0] + (pData[1] << 8);
					b += 2;

					ptr[colorSwizzleTable[2]] = (((color & 0x7C00) >> 10) << 3);
					ptr[colorSwizzleTable[1]] = (((color & 0x03E0) >>  5) << 3);
					ptr[colorSwizzleTable[0]] = (((color & 0x001F) >>  0) << 3);
				}
			}
		}
	}

	private void readTga24bitsRLE(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		uint b = 0;
		ubyte * ptr = pixels.ptr;
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		while (ptr < pixels.ptr + (width * height) * 3)
		{
			// Read first byte
			const ubyte packetHeader = data[b++];
			const uint  size = 1 + (packetHeader & 0x7F);

			if (packetHeader & 0x80)
			{
				// Run-length packet
				const(ubyte *) pData = data + b;
				for (uint i = 0; i < size; ++i, ptr += 3)
				{
					ptr[0] = pData[colorSwizzleTable[0]];
					ptr[1] = pData[colorSwizzleTable[1]];
					ptr[2] = pData[colorSwizzleTable[2]];
				}
				b += 3;
			}
			else
			{
				// Non run-length packet
				for (uint i = 0; i < size; ++i, ptr += 3)
				{
					const(ubyte *) pData = data + b;
					ptr[0] = pData[colorSwizzleTable[0]];
					ptr[1] = pData[colorSwizzleTable[1]];
					ptr[2] = pData[colorSwizzleTable[2]];
					b += 3;
				}
			}
		}
	}

	private void readTga32bitsRLE(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		uint b = 0;
		ubyte * ptr = pixels.ptr;
		immutable(uint *) colorSwizzleTable = getColorSwizzleTable();

		while (ptr < pixels.ptr + (width * height) * 4)
		{
			// Read first byte
			const ubyte packetHeader = data[b++];
			const uint  size = 1 + (packetHeader & 0x7F);

			if (packetHeader & 0x80)
			{
				// Run-length packet
				const(ubyte *) pData = data + b;
				for (uint i = 0; i < size; ++i, ptr += 4)
				{
					ptr[0] = pData[colorSwizzleTable[0]];
					ptr[1] = pData[colorSwizzleTable[1]];
					ptr[2] = pData[colorSwizzleTable[2]];
					ptr[3] = pData[colorSwizzleTable[3]];
				}
				b += 4;
			}
			else
			{
				// Non run-length packet
				for (uint i = 0; i < size; ++i, ptr += 4)
				{
					const(ubyte *) pData = data + b;
					ptr[0] = pData[colorSwizzleTable[0]];
					ptr[1] = pData[colorSwizzleTable[1]];
					ptr[2] = pData[colorSwizzleTable[2]];
					ptr[3] = pData[colorSwizzleTable[3]];
					b += 4;
				}
			}
		}
	}

	private void readTgaGray8bitsRLE(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		uint b = 0;
		ubyte * ptr = pixels.ptr;

		while (ptr < pixels.ptr + (width * height))
		{
			// Read first byte
			const ubyte packetHeader = data[b++];
			const uint  size = 1 + (packetHeader & 0x7F);

			if (packetHeader & 0x80)
			{
				// Run-length packet
				const ubyte color = data[b++];
				memset(ptr, color, size);
				ptr += size;
			}
			else
			{
				// Non run-length packet
				memcpy(ptr, (data + b), size);
				ptr += size;
				b   += size;
			}
		}
	}

	private void readTgaGray16bitsRLE(const uint width, const uint height, ubyte[] pixels, const(ubyte *) data) const
	{
		assert(data !is null);

		uint b = 0;
		ubyte * ptr = pixels.ptr;

		while (ptr < pixels.ptr + (width * height))
		{
			// Read first byte
			const ubyte packetHeader = data[b++];
			const uint  size = 1 + (packetHeader & 0x7F);

			if (packetHeader & 0x80)
			{
				// Run-length packet
				for (uint i = 0; i < size; ++i, ++ptr)
				{
					*ptr = data[b];
				}
				b += 2; // Skip the alpha byte
			}
			else
			{
				// Non run-length packet
				for (uint i = 0; i < size; ++i, ++ptr, b += 2)
				{
					*ptr = data[b];
				}
			}
		}
	}
}
