
// ================================================================================================
// -*- D -*-
// File: Camera.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: First-person 3D camera.
// ================================================================================================

module deferred.Camera;

public import deferred.MathUtils;

// ========================================================
// Camera class:
// ========================================================

//
// Simple first-person 3D camera.
//
// Initial Camera Axes:
//
//  (up)
//  +Y   +Z (forward)
//  |   /
//  |  /
//  | /
//  + ------ +X (right)
//
// The methods you will want to call in the application are:
//  - update()
//  - keyInput()
//  - mouseInput()
//  - clearInput()
// The others are mostly for internal use.
//
// Movement keys are [W],[A],[S],[D]
// and mouse motion to look around.
//
class Camera
{
	// Directions to by with Camera::move().
	enum MoveDir
	{
		Forward,
		Back,
		Left,
		Right
	}

	// Camera states:
	private
	{
		Vec3 right   = Vec3(1.0f, 0.0f, 0.0f); // The normalized axis that points to the "right"
		Vec3 up      = Vec3(0.0f, 1.0f, 0.0f); // The normalized axis that points "up"
		Vec3 forward = Vec3(0.0f, 0.0f, 1.0f); // The normalized axis that points "forward"
		Vec3 eye     = Vec3(0.0f, 1.0f, 0.0f); // The position of the camera (i.e. the camera's eye and origin of the camera's coordinate system)

		// Camera movement and rotation speed:
		float rotationSpeed = 2.0f * (1.0f / 30.0f);
		float movementSpeed = 2.0f * (1.0f / 30.0f);
		int   edgeRotationSpeed = 3;

		// Projection parameters (with defaults):
		float fovyDegrees    = 60.0f;
		float zNear          = 0.1f;
		float zFar           = 1000.0f;
		int   viewportWidth  = 1024;
		int   viewportHeight = 768;

		// Virtual keys we care about:
		struct Keys
		{
			bool w;
			bool a;
			bool s;
			bool d;
		};
		Keys keyMap;

		// Current mouse/cursor position:
		int oldCursorPosX     = 0;
		int oldCursorPosY     = 0;
		int currentCursorPosX = 0;
		int currentCursorPosY = 0;

		// Pitch angle history:
		float pitchAmt = 0.0f;
	}

	// ----------------------------------------------------

	void setViewport(int width, int height)
	{
		viewportWidth  = width;
		viewportHeight = height;

		oldCursorPosX     = viewportWidth  / 2;
		oldCursorPosY     = viewportHeight / 2;
		currentCursorPosX = viewportWidth  / 2;
		currentCursorPosY = viewportHeight / 2;
	}

	void setEyeOrigin(float x, float y, float z)
	{
		eye.x = x;
		eye.y = y;
		eye.z = z;
	}

	Vec3 getEyeOrigin() const
	{
		return eye;
	}

	// This function returns what the camera is looking at. Our eye is ALWAYS the origin
	// of camera's coordinate system and we are ALWAYS looking straight down the "forward" axis
	// so to calculate the target it's just a matter of adding the eye plus the forward.
	Vec3 getTarget() const
	{
		return Vec3(eye.x + forward.x, eye.y + forward.y, eye.z + forward.z);
	}

	// Build and return the 4x4 camera view matrix.
	Mat4 getViewMatrix() const
	{
		Mat4 lookMatrix;
		const Vec3 lookAtVec = getTarget();
		lookMatrix.setLookAt(eye, lookAtVec, up);
		return lookMatrix;
	}

	// Get a perspective projection matrix that can be combined with the camera's view matrix.
	Mat4 getProjectionMatrix() const
	{
		Mat4 projectionMatrix;
		projectionMatrix.setPerspectiveFov(fovyDegrees, cast(float)viewportWidth / cast(float)viewportHeight, zNear, zFar);
		return projectionMatrix;
	}

	// Convenience method that return the view * projection matrix.
	Mat4 getViewProjectionMatrix() const
	{
		const Mat4 p = getProjectionMatrix();
		const Mat4 v = getViewMatrix();
		return Mat4.mul(p, v);
	}

	// Resets to a starting position.
	void reset(Vec3 rightVec, Vec3 upVec, Vec3 forwardVec, Vec3 eyeVec)
	{
		right   = rightVec;
		up      = upVec;
		forward = forwardVec;
		eye     = eyeVec;
	}

	// Pitches camera by an angle in degrees (tilt it up/down).
	void pitch(float degrees)
	{
		// Calculate new forward:
		forward = rotateAroundAxis(forward, right, degrees);

		// Calculate new camera up vector:
		up = forward.cross(right);
	}

	// Rotates around world Y-axis by the given angle (in degrees).
	void rotate(float degrees)
	{
		const float radians = degToRad(degrees);
		const float sinAng  = std.math.sin(radians);
		const float cosAng  = std.math.cos(radians);

		// Save off forward components for computation:
		float xxx = forward.x;
		float zzz = forward.z;

		// Rotate forward vector:
		forward.x = xxx *  cosAng + zzz * sinAng;
		forward.z = xxx * -sinAng + zzz * cosAng;

		// Save off up components for computation:
		xxx = up.x;
		zzz = up.z;

		// Rotate up vector:
		up.x = xxx *  cosAng + zzz * sinAng;
		up.z = xxx * -sinAng + zzz * cosAng;

		// Save off right components for computation:
		xxx = right.x;
		zzz = right.z;

		// Rotate right vector:
		right.x = xxx *  cosAng + zzz * sinAng;
		right.z = xxx * -sinAng + zzz * cosAng;
	}

	// Moves the camera by the given direction, using the default movement speed.
	// The last three parameters indicate in which axis to move.
	// If it is equal to 1, move in that axis, if it is zero don't move.
	void move(MoveDir dir, float x, float y, float z)
	{
		if (dir == MoveDir.Forward) // Move along the camera's forward vector:
		{
			eye.x += (forward.x * movementSpeed) * x;
			eye.y += (forward.y * movementSpeed) * y;
			eye.z += (forward.z * movementSpeed) * z;
		}
		else if (dir == MoveDir.Back) // Move along the camera's negative forward vector:
		{
			eye.x -= (forward.x * movementSpeed) * x;
			eye.y -= (forward.y * movementSpeed) * y;
			eye.z -= (forward.z * movementSpeed) * z;
		}
		else if (dir == MoveDir.Left) // Move along the camera's negative right vector:
		{
			eye.x += (right.x * movementSpeed) * x;
			eye.y += (right.y * movementSpeed) * y;
			eye.z += (right.z * movementSpeed) * z;
		}
		else if (dir == MoveDir.Right) // Move along the camera's right vector:
		{
			eye.x -= (right.x * movementSpeed) * x;
			eye.y -= (right.y * movementSpeed) * y;
			eye.z -= (right.z * movementSpeed) * z;
		}
		else
		{
			assert(false && "Invalid camera move direction!");
		}
	}

	// This allows us to rotate 'vec' around an arbitrary axis by an angle in degrees.
	Vec3 rotateAroundAxis(Vec3 vec, Vec3 axis, float degrees) const
	{
		const float radians = degToRad(degrees);
		const float sinAng  = std.math.sin(radians);
		const float cosAng  = std.math.cos(radians);

		const float oneMinusCosAng = (1.0f - cosAng);
		const float aX = axis.x;
		const float aY = axis.y;
		const float aZ = axis.z;

		float xxx = (aX * aX * oneMinusCosAng + cosAng)      * vec.x +
					(aX * aY * oneMinusCosAng + aZ * sinAng) * vec.y +
					(aX * aZ * oneMinusCosAng - aY * sinAng) * vec.z;

		float yyy = (aX * aY * oneMinusCosAng - aZ * sinAng) * vec.x +
					(aY * aY * oneMinusCosAng + cosAng)      * vec.y +
					(aY * aZ * oneMinusCosAng + aX * sinAng) * vec.z;

		float zzz = (aX * aZ * oneMinusCosAng + aY * sinAng) * vec.x +
					(aY * aZ * oneMinusCosAng - aX * sinAng) * vec.y +
					(aZ * aZ * oneMinusCosAng + cosAng)      * vec.z;

		return Vec3(xxx, yyy, zzz);
	}

	// Updates camera based on mouse movement.
	// Called internally by update().
	void look()
	{
		int deltaX, deltaY;

		if (currentCursorPosX >= (viewportWidth - 50))
		{
			deltaX = edgeRotationSpeed;
		}
		else if (currentCursorPosX <= 50)
		{
			deltaX = -edgeRotationSpeed;
		}
		else
		{
			deltaX = currentCursorPosX - oldCursorPosX;
		}

		if (currentCursorPosY >= (viewportHeight - 50))
		{
			deltaY = edgeRotationSpeed;
		}
		else if (currentCursorPosY <= 50)
		{
			deltaY = -edgeRotationSpeed;
		}
		else
		{
			deltaY = currentCursorPosY - oldCursorPosY;
		}

		if (deltaX == 0 && deltaY == 0)
		{
			// No change since last update.
			return;
		}

		auto clamp = (int x, int minimum, int maximum)
		{
			return (x < minimum) ? minimum : (x > maximum) ? maximum : x;
		};

		// Cap the mouse delta to avoid abrupt view movement.
		immutable int deltaCap = 50;

		// Max pitch angle to avoid a "Gimbal Lock":
		immutable float maxAngle = 89.0f;

		// If the user clicks the edges of the screen, the delta
		// value will be huge and the camera would jump. This is
		// a crude but effective way of preventing that.
		deltaX = clamp(deltaX, -deltaCap, +deltaCap);
		deltaY = clamp(deltaY, -deltaCap, +deltaCap);

		oldCursorPosX = currentCursorPosX;
		oldCursorPosY = currentCursorPosY;

		// Rotate left/right:
		float amt = cast(float)deltaX * rotationSpeed;
		rotate(-amt);

		// Calculate amount to rotate up/down:
		amt = cast(float)deltaY * rotationSpeed;

		// Clamp pitch amount:
		if ((pitchAmt + amt) <= -maxAngle)
		{
			amt = -maxAngle - pitchAmt;
			pitchAmt = -maxAngle;
		}
		else if ((pitchAmt + amt) >= maxAngle)
		{
			amt = maxAngle - pitchAmt;
			pitchAmt = maxAngle;
		}
		else
		{
			pitchAmt += amt;
		}

		// Pitch camera:
		pitch(-amt);
	}

	void clearInput()
	{
		keyMap.w = false;
		keyMap.a = false;
		keyMap.s = false;
		keyMap.d = false;
	}

	void keyInput(char keyChar, bool isKeyDown)
	{
		switch (keyChar)
		{
		case 'W' : keyMap.w = isKeyDown; break;
		case 'A' : keyMap.a = isKeyDown; break;
		case 'S' : keyMap.s = isKeyDown; break;
		case 'D' : keyMap.d = isKeyDown; break;
		default  : break;
		} // End switch (keyChar)
	}

	void mouseInput(int mx, int my)
	{
		currentCursorPosX = mx;
		currentCursorPosY = my;
	}

	// Updates the camera based on input received.
	// Should be called every frame.
	void update()
	{
		if (keyMap.w)
		{
			move(MoveDir.Forward, 1.0f, 1.0f, 1.0f);
		}
		if (keyMap.a)
		{
			move(MoveDir.Left, 1.0f, 1.0f, 1.0f);
		}
		if (keyMap.s)
		{
			move(MoveDir.Back, 1.0f, 1.0f, 1.0f);
		}
		if (keyMap.d)
		{
			move(MoveDir.Right, 1.0f, 1.0f, 1.0f);
		}
		look();
	}
}
