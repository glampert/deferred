
// ================================================================================================
// -*- D -*-
// File: Application.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-06
// Brief: Application interface.
// ================================================================================================

module deferred.Application;

public import deferred.Renderer;

// ========================================================
// LaunchOptions structure:
// ========================================================

struct LaunchOptions
{
	// Window dimensions:
	uint windowWidth;
	uint windowHeight;

	// Framebuffer dimensions, might
	// differ from the window size.
	uint framebufferWidth;
	uint framebufferHeight;

	// If > 0, tries to enable multisampling.
	uint numSamples;

	// Other misc flags:
	bool vsync;             // Enable vertical-sync
	bool runningFullScreen; // Run in fullscreen mode
	bool verbose;           // Enable verbose logging
}

// ========================================================
// Application interface:
// ========================================================

interface Application
{
	// Initialize the whole application and parse command line args.
	void initWithCommandLine(string[] cmdArgs);

	// Runs the application main loop.
	void run();

	// Shutdown before main() exits.
	void shutdown();

	// Get a read-only reference to the launch options parsed from command line.
	const(LaunchOptions *) getLaunchOptions() const;

	// Get a reference to the underlaying renderer module.
	Renderer getRenderer();

	// Application events:
	void onMouseMotion(float x, float y);
	void onKeyUp(int keyCode, int modifiers);
	void onKeyDown(int keyCode, int modifiers);
}
