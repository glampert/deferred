
// ================================================================================================
// -*- D -*-
// File: ImageLoader.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-24
// Brief: Image/texture file loader interface.
// ================================================================================================

module deferred.ImageLoader;

private
{
	import deferred.Common;
	import deferred.Texture;
	import deferred.TgaImageLoader;
	static import std.path;
	static import std.string;
}

// ========================================================
// ImageData structure:
// ========================================================

// Basic information pertaining an image:
struct ImageData
{
	uint width;
	uint height;
	uint numColorComponents;
	Texture.PixelFormat pixelFormat;
	ubyte[] pixels;
}

// ========================================================
// ImageLoader interface:
// ========================================================

interface ImageLoader
{
	// Create a loader or throws an exception if no proper loader can be found.
	static ImageLoader createLoaderForFile(const string filename)
	{
		assert(filename !is null);

		// Extract filename extension:
		const string ext = std.path.extension(filename);
		if (ext is null || (ext.length == 0))
		{
			throw new Exception(std.string.format("No identifiable extension in filename \"%s\"", filename));
		}

		// Test extension against the available image loaders:
		if (std.string.icmp(ext, ".tga") == 0)
		{
			Common.logComment("Creating TGA image loader for file(s) \"", filename, "\"...");
			return new TgaImageLoader();
		}

		// No suitable loader found.
		throw new Exception(std.string.format("No loader found for image file \"%s\"", filename));
	}

	// Loads the image from a file. Fill 'image' on success. Throws an exception on failure.
	void loadImageFromFile(const string filename, ref ImageData image);

	// Loads the image from a memory block. Fill 'image' on success. Throws an exception on failure.
	void loadImageFromMemory(const ubyte[] memory, ref ImageData image);

	// Set a given loader property by its name. Returns false if the property doesn't exist.
	bool loaderSetPropertyInt(const string propName, int value);

	// Get a given loader property by its name. Returns false if the property doesn't exist.
	bool loaderGetPropertyInt(const string propName, ref int value) const;
}
