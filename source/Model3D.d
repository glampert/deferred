
// ================================================================================================
// -*- D -*-
// File: Model3d.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: Basic classes and structures defined a 3D model/object.
// ================================================================================================

module deferred.Model3d;

public import deferred.MathUtils;

// ========================================================
// DrawVertex structure:
// ========================================================

struct DrawVertex
{
	Vec3 position;
	Vec3 normal;
	Vec3 tangent;
	Vec3 bitangent;
	float u, v;

	// Attribute indexes for glEnableVertexAttribArray():
	enum PositionAttrIndex  = 0;
	enum NormalAttrIndex    = 1;
	enum TangentAttrIndex   = 2;
	enum BiTangentAttrIndex = 3;
	enum TexCoordsAttrIndex = 4;

	// Offsets for glVertexAttribPointer():
	enum PositionOffset  = Vec3.sizeof * 0;
	enum NormalOffset    = Vec3.sizeof * 1;
	enum TangentOffset   = Vec3.sizeof * 2;
	enum BiTangentOffset = Vec3.sizeof * 3;
	enum TexCoordsOffset = Vec3.sizeof * 4;
}

// Adjust this if DrawVertex is changed!
static assert(DrawVertex.sizeof == (Vec3.sizeof * 4 + float.sizeof * 2));

// ========================================================
// MeshBufferStorage enum:
// ========================================================

enum MeshBufferStorage
{
	Static,
	Dynamic,
	Stream
}

// ========================================================
// SubMesh3d structure:
// ========================================================

struct SubMesh3d
{
	// System-memory-side data:
	DrawVertex[] vertexes;
	uint[] indexes;

	// Offsets in VB/IB, needed for rendering.
	uint firstVertexInVB;
	uint firstIndexInIB;
}

// ========================================================
// Model3d interface:
// ========================================================

interface Model3d
{
	// Load model from a file. Throws an exception on error.
	void loadFromFile(const string filename, MeshBufferStorage storage);

	// Initializes a 3D model from a set of sub-meshes.
	void initFromData(SubMesh3d[] meshSet, MeshBufferStorage storage, const string name);

	// Initialize this model to a sphere.
	void makeSphereModel(uint numZSamples, uint numRadialSamples, float radius, MeshBufferStorage storage);

	// Re-create VB/IB, etc.
	void refreshGpuBuffers(MeshBufferStorage storage);

	// Manually frees all model data, including the GPU buffers.
	void freeAllData();

	// Renders the whole model Vertex Buffer using
	// whichever shader program and material currently enabled.
	void draw() const;

	// Pre-allocate sub-mesh storage:
	void allocateSubMeshes(uint num);
	uint geSubMeshCount() const;

	// Data accessors:
	string getOriginalFileName() const;

	uint  getVertexCount() const;
	uint  getIndexCount() const;
	ulong getVertexBufferSizeBytes() const;
	ulong getIndexBufferSizeBytes() const;

	SubMesh3d * getSubMesh(uint index);
	const(SubMesh3d *) getSubMesh(uint index) const;
}
