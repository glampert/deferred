
// ================================================================================================
// -*- D -*-
// File: Model3dImporter.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-15
// Brief: Asset importer and 3D model manager. Relies on the ASSIMP library.
// ================================================================================================

module deferred.Model3dImporter;

private
{
	import deferred.Common;
	import deferred.MathUtils;
	import deferred.Model3d;
	import derelict.assimp3.assimp;
	static import std.array;
	static import std.string;
}

// ========================================================
// Triangle structure:
// ========================================================

struct Triangle
{
	uint vertIndex[3];

	void setIndexes(uint v0, uint v1, uint v2)
	{
		vertIndex[0] = v0;
		vertIndex[1] = v1;
		vertIndex[2] = v2;
	}
};

// ========================================================
// Model3dImporter singleton:
// ========================================================

class Model3dImporter
{
	private
	{
		// We keep this flag to avoid initializing the ASSIMP library more than once.
		static bool assimpLoaded = false;

		// Log object provided to ASSIMP.
		static aiLogStream assimpLog;

		// Default ASSIMP post processing flags used.
		static immutable uint defaultASSIMPImportFlags =
		(
			aiProcess_Triangulate              |
			aiProcess_JoinIdenticalVertices    |
			aiProcess_RemoveRedundantMaterials |
			aiProcess_OptimizeMeshes           |
			aiProcess_OptimizeGraph            |
			aiProcess_ImproveCacheLocality     |
			aiProcess_GenSmoothNormals         |
			aiProcess_CalcTangentSpace
		);
	}

	// ----------------------------------------------------

	static void importModel3dFromFile(const string filename, Model3d mdl)
	{
		assert(mdl      !is null);
		assert(filename !is null);
		assert(filename.length != 0);

		if (!assimpLoaded) // One time init
		{
			initASSIMP();
		}

		Common.logComment("Beginning import for Model3d \"", filename, "\"...");

		const(aiScene *) scene = aiImportFile(std.string.toStringz(filename), defaultASSIMPImportFlags);
		scope(exit)
		{
			if (scene !is null)
			{
				aiReleaseImport(scene);
			}
		}

		// A null scene is probably a result of an inexistent file.
		if (scene is null)
		{
			throw new Exception(std.string.format("Failed to import 3D model from file \"%s\"", filename));
		}

		// Each aiScene will output 1 Model3d, with 1..N SubMesh3Ds.
		if (scene.mNumMeshes == 0)
		{
			throw new Exception(std.string.format("\"%s\" scene file had no meshes in it!", filename));
		}

		importModelFromAiScene(mdl, scene);
	}

	private static void importModelFromAiScene(Model3d mdl, const(aiScene *) scene)
	{
		mdl.allocateSubMeshes(scene.mNumMeshes);
		const uint subMeshCount = mdl.geSubMeshCount();
		assert(subMeshCount == scene.mNumMeshes);

		// Sub-mesh vertex array ranges are relative to the Mesh range in the array.
		uint meshVertexCount = 0, meshIndexCount = 0;
		uint meshBaseVertex  = 0, meshBaseIndex  = 0;

		for (uint i = 0; i < subMeshCount; ++i)
		{
			SubMesh3d * dest = mdl.getSubMesh(i);
			const(aiMesh *) source = scene.mMeshes[i];

			meshVertexCount = source.mNumVertices;
			meshIndexCount  = source.mNumFaces * 3; // Triangles

			dest.vertexes.length = meshVertexCount;
			dest.indexes.length  = meshIndexCount;
			dest.firstVertexInVB = meshBaseVertex;
			dest.firstIndexInIB  = meshBaseIndex;

			// Assemble vertex/index array for each sub-mesh in the scene:
			processMesh(source, dest);

			// Get material for surface:
			processMaterial(scene.mMaterials[source.mMaterialIndex], dest);

			// Next surface:
			meshBaseVertex += meshVertexCount;
			meshBaseIndex  += meshIndexCount;
		}

		Common.logComment("Finished importing Model3d...");
	}

	private static void processMesh(const(aiMesh *) source, SubMesh3d * dest)
	{
		Common.logComment("Processing mesh \"", aiStringConv(source.mName), "\"...");

		// Process vertexes:
		dest.vertexes.length = source.mNumVertices;
		for (uint v = 0; v < source.mNumVertices; ++v)
		{
			DrawVertex * dv = &dest.vertexes[v];

			const(aiVector3D *) P = &source.mVertices[v];
			dv.position.x = P.x;
			dv.position.y = P.y;
			dv.position.z = P.z;

			const(aiVector3D *) T = &source.mTangents[v];
			dv.tangent.x = T.x;
			dv.tangent.y = T.y;
			dv.tangent.z = T.z;

			const(aiVector3D *) B = &source.mBitangents[v];
			dv.bitangent.x = B.x;
			dv.bitangent.y = B.y;
			dv.bitangent.z = B.z;

			const(aiVector3D *) N = &source.mNormals[v];
			dv.normal.x = N.x;
			dv.normal.y = N.y;
			dv.normal.z = N.z;

			// Some bogus meshes don't have texture coords,
			// which would cause a null pointer access without this check.
			if (source.mTextureCoords[0] !is null)
			{
				const aiVector3D * tx = &source.mTextureCoords[0][v];
				dv.u = tx.x;
				dv.v = tx.y;
			}
			else
			{
				dv.u = 0.0f;
				dv.v = 0.0f;
			}
		}

		// Copy indexes:
		dest.indexes.length = (source.mNumFaces * 3); // We only operate on triangle meshes.
		for (uint t = 0; t < source.mNumFaces; ++t)
		{
			const(aiFace *) face = &source.mFaces[t];
			assert(face.mNumIndices == 3); // Should be triangulated, but check nevertheless.

			dest.indexes[(t * 3) + 0] = face.mIndices[0];
			dest.indexes[(t * 3) + 1] = face.mIndices[1];
			dest.indexes[(t * 3) + 2] = face.mIndices[2];
		}
	}

	private static void processMaterial(const(aiMaterial *) mat, SubMesh3d * dest)
	{
		//TODO
	}

	private static string aiStringConv(const ref aiString s)
	{
		if (s.length == 0)
		{
			return "";
		}

		char tmp[];
		for (size_t i = 0; i < s.length; ++i)
		{
			tmp ~= s.data[i];
		}
		tmp ~= '\0';

		return std.conv.to!string(tmp);
	}

	private static void initASSIMP()
	{
		DerelictASSIMP3.load();
		assimpLoaded = true;

		Common.logComment("ASSIMP library loaded! Lib version: ",
				aiGetVersionMajor(), ".", aiGetVersionMinor(), ".", aiGetVersionRevision());

		// Register log callback:
		assimpLog.user = null;
		assimpLog.callback = &ASSIMPLogCallback;
		aiAttachLogStream(&assimpLog);
	}

	private static float orthonormalize(Vec3[] v)
	{
		float minLength = v[0].length();
		v[0] = v[0].normalized();

		for (int i = 1; i < 3; ++i)
		{
			for (int j = 0; j < i; ++j)
			{
				const float d  = v[i].dot(v[j]);
				const Vec3 tmp = Vec3.mul(v[j], d);
				v[i] = Vec3.sub(v[i], tmp);
			}

			const float len = v[i].length();
			v[i] = v[i].normalized();
			if (len < minLength)
			{
				minLength = len;
			}
		}
		return minLength;
	}

	private static float computeOrthogonalComplement(Vec3[] v)
	{
		if (std.math.abs(v[0][0]) > std.math.abs(v[0][1]))
		{
			v[1] = Vec3(-v[0][2], 0.0f, +v[0][0]);
		}
		else
		{
			v[1] = Vec3(0.0f, +v[0][2], -v[0][1]);
		}

		v[2] = v[0].cross(v[1]);
		return orthonormalize(v);
	}

	static void createSphere(uint numZSamples, uint numRadialSamples, float radius,
	                         ref DrawVertex[] verts, ref Triangle[] triangles)
	{
		immutable float twoPi = 2.0f * std.math.PI;

		uint zsm1 = numZSamples - 1;
		uint zsm2 = numZSamples - 2;
		uint zsm3 = numZSamples - 3;

		uint rsp1         = numRadialSamples + 1;
		uint numVerts     = zsm2 * rsp1 + 2;
		uint numTriangles = 2 * zsm2 * numRadialSamples;

		float invRS   = 1.0f / cast(float)numRadialSamples;
		float zFactor = 2.0f / cast(float)zsm1;

		verts     = new DrawVertex[numVerts];
		triangles = new Triangle[numTriangles];

		Vec3 pos, nor;
		Vec3[3] basis;
		float[2] tc;

		// Generate points on the unit circle to be used in
		// computing the mesh points on a sphere slice.
		auto cs = new float[rsp1];
		auto sn = new float[rsp1];
		for (uint r = 0; r < numRadialSamples; ++r)
		{
			float angle = invRS * r * twoPi;
			cs[r] = std.math.cos(angle);
			sn[r] = std.math.sin(angle);
		}

		cs[numRadialSamples] = cs[0];
		sn[numRadialSamples] = sn[0];

		// Generate the sphere itself:
		uint i = 0;
		for (uint z = 1; z < zsm1; ++z)
		{
			float zFraction = -1.0f + zFactor * cast(float)z; // in (-1,1)
			float zValue    = radius * zFraction;

			// Compute center of slice:
			const Vec3 sliceCenter = Vec3(0.0f, 0.0f, zValue);

			// Compute radius of slice:
			float sliceRadius = std.math.sqrt(std.math.abs((radius * radius) - (zValue * zValue)));

			// Compute slice vertexes with duplication at endpoint:
			for (uint r = 0; r <= numRadialSamples; ++r, ++i)
			{
				float radialFraction = r * invRS; // in [0,1)
				const Vec3 radial = Vec3(cs[r], sn[r], 0.0f);

				const Vec3 tmp = Vec3.mul(radial, sliceRadius);
				pos = Vec3.add(sliceCenter, tmp);
				nor = pos;
				nor.normalizeSelf();

				basis[0] = nor;
				computeOrthogonalComplement(basis);
				tc[0] = radialFraction;
				tc[1] = 0.5f * (zFraction + 1.0f);

				verts[i].position  = pos;
				verts[i].normal    = nor;
				verts[i].tangent   = basis[1];
				verts[i].bitangent = basis[2];
				verts[i].u         = tc[0];
				verts[i].v         = tc[1];
			}
		}

		// The point at the south pole:
		pos = Vec3(0.0f, 0.0f, -radius);
		nor = Vec3(0.0f, 0.0f, -1.0f);

		basis[0] = nor;
		computeOrthogonalComplement(basis);
		tc[0] = 0.5f;
		tc[1] = 0.5f;
		verts[i].position  = pos;
		verts[i].normal    = nor;
		verts[i].tangent   = basis[1];
		verts[i].bitangent = basis[2];
		verts[i].u         = tc[0];
		verts[i].v         = tc[1];
		++i;

		// The point at the north pole:
		pos = Vec3(0.0f, 0.0f, radius);
		nor = Vec3(0.0f, 0.0f, 1.0f);

		basis[0] = nor;
		computeOrthogonalComplement(basis);
		tc[0] = 0.5f;
		tc[1] = 1.0f;
		verts[i].position  = pos;
		verts[i].normal    = nor;
		verts[i].tangent   = basis[1];
		verts[i].bitangent = basis[2];
		verts[i].u         = tc[0];
		verts[i].v         = tc[1];

		// Generate index buffer (outside view).
		uint t = 0;
		for (uint z = 0, zStart = 0; z < zsm3; ++z)
		{
			uint i0 = zStart;
			uint i1 = i0 + 1;
			zStart += rsp1;
			uint i2 = zStart;
			uint i3 = i2 + 1;

			for (i = 0; i < numRadialSamples; ++i, ++i0, ++i1, ++i2, ++i3)
			{
				triangles[t++].setIndexes(i0, i1, i2);
				triangles[t++].setIndexes(i1, i3, i2);
			}
		}

		// The south pole triangles (outside view).
		uint numVerticesM2 = numVerts - 2;
		for (i = 0; i < numRadialSamples; ++i, ++t)
		{
			triangles[t].setIndexes(i, numVerticesM2, i + 1);
		}

		// The north pole triangles (outside view).
		uint numVerticesM1 = numVerts - 1;
		uint offset = zsm3 * rsp1;
		for (i = 0; i < numRadialSamples; ++i, ++t)
		{
			triangles[t].setIndexes(i + offset, i + 1 + offset, numVerticesM1);
		}
	}

	// Disable external instantiation (singleton).
	private this() { }
}

// ========================================================

extern(C)
{
	private void ASSIMPLogCallback(const(char) * message, char *) nothrow
	{
		try
		{
			Common.logComment("ASSIMP log: ", Common.oneliner(std.conv.to!string(message)));
		}
		catch (Exception) { }
	}
}
