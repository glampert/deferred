
// ================================================================================================
// -*- D -*-
// File: Light.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: Lighting properties object.
// ================================================================================================

module deferred.Light;

public
{
	import deferred.MathUtils;
	import deferred.Material;
}

// ========================================================
// Light class:
// ========================================================

class Light
{
	// Supported light types.
	enum Type
	{
		// A point light has a well-defined position in space
		// but no direction - it emits light in all directions.
		Point,

		// Number of entries. Internal use.
		Count
	};

	private
	{
		// Type of light source: point, directional, spot, etc.
		Type type;

		// World position and color.
		Vec3   wPos;
		Color4 color;

		// Anything outside this radius is not affected by a point light.
		// The attenuation constants for a point light can be computed from
		// this value, using the formula:
		//   pointAttenConst     = 1;
		//   pointAttenLinear    = 2 / pointLightRadius;
		//   pointAttenQuadratic = 1 / (pointLightRadius * pointLightRadius);
		float pointLightRadius;

		// Light attenuation computed as:
		//   attenuation = 1 / (pointAttenConst + pointAttenLinear * d + pointAttenQuadratic * d*d)
		// For a given distance 'd' from the light's position.
		float pointAttenConst;     // Constant light attenuation factor.
		float pointAttenLinear;    // Linear light attenuation factor.
		float pointAttenQuadratic; // Quadratic light attenuation factor.
	}

	@property
	{
		Type lightType() const { return type; }
		Type lightType(Type t) { return type = t; }

		Vec3 worldPosition() const { return wPos; }
		Vec3 worldPosition(Vec3 p) { return wPos = p; }

		Color4 lightColor() const   { return color; }
		Color4 lightColor(Color4 c) { return color = c; }

		float pointRadius() const  { return pointLightRadius; }
		float pointRadius(float r) { return pointLightRadius = r; }

		float pointAttenuationConst() const  { return pointAttenConst; }
		float pointAttenuationConst(float a) { return pointAttenConst = a; }

		float pointAttenuationLinear() const  { return pointAttenLinear; }
		float pointAttenuationLinear(float a) { return pointAttenLinear = a; }

		float pointAttenuationQuadratic() const  { return pointAttenQuadratic; }
		float pointAttenuationQuadratic(float a) { return pointAttenQuadratic = a; }
	}

	void setPointLightRadiusAndDeriveAttenuations(float radius)
	{
		pointLightRadius    = radius;
		pointAttenConst     = 1.0f;
		pointAttenLinear    = 2.0f / radius;
		pointAttenQuadratic = 1.0f / (radius * radius);
	}
}
