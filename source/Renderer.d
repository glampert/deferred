
// ================================================================================================
// -*- D -*-
// File: Renderer.d
// Author: Guilherme R. Lampert
// Created on: 2014-06-07
// Brief: Renderer interface.
// ================================================================================================

module deferred.Renderer;

public
{
	import deferred.Application;
	import deferred.GBuffer;
	import deferred.Light;
	import deferred.Material;
	import deferred.MathUtils;
	import deferred.Model3d;
	import deferred.ShaderProgram;
	import deferred.Texture;
}

// ========================================================
// Renderer interface:
// ========================================================

interface Renderer
{
	// Factory methods:
	ShaderProgram createShaderProgram();
	Texture createTexture();
	Model3d createModel3d();
	GBuffer createGBuffer();

	void initWithOptions(const ref LaunchOptions options);
	void shutdown();

	const(LaunchOptions *) getLaunchOptions() const;
	uint getMaxTextureAnisotropy() const;

	void   setShaderSourcePath(const string path);
	string getShaderSourcePath() const;

	// Check or clear OpenGL errors (by calling glGetError).
	void clearErrors() const;
	void checkErrors(bool throwException) const;

	// Draws a fullscreen NDC quadrilateral over the current scene (with depth disabled).
	// ShaderProgram may be null to use a default one.
	void drawFullscreenQuadrilateral(in ShaderProgram program = null);

	// Get a 3D model for a light, which best represents its volume in 3D space.
	const(Model3d) getLightMesh(in Light light) const;
}
