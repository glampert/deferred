
## Deferre.d - A simple deferred renderer written in DLang

Personal project mainly intended at learning the D programing language.

![deferre.d](https://bytebucket.org/glampert/deferred/raw/175745d36f595ed60918f40fe3f55d08a8566749/deferred.png "deferre.d")

### License

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

