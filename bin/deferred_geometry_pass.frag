
uniform sampler2D u_diffuse_samp; // tmu:0
uniform sampler2D u_normal_samp;  // tmu:1

//layout(location = 0) in vec3 v_world_pos;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec3 v_tangent;
layout(location = 3) in vec3 v_bitangent;
//layout(location = 4) in vec4 v_color;
layout(location = 5) in vec2 v_texcoords;

layout(location = 0) out vec3 rt_diffuseColor;
layout(location = 1) out vec3 rt_normalVector;

void main()
{
	mat3 TBN    = mat3(v_tangent, v_bitangent, v_normal);
	vec3 normal = normalize(TBN * ((texture(u_normal_samp, v_texcoords).rgb * 2.0) - 1.0));

	//TODO v_color * texture_color!

	rt_diffuseColor = texture(u_diffuse_samp, v_texcoords).rgb;
	rt_normalVector = (normal.xyz * 0.5) + 0.5;

//	rt_normalVector = texture(u_normal_samp, v_texcoords).rgb;
}
