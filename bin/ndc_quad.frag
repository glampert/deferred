
uniform sampler2D u_texture_samp;

layout(location = 0) in vec2 v_texcoords;

out vec4 frag_color;

void main()
{
	frag_color = texture(u_texture_samp, v_texcoords);
}
