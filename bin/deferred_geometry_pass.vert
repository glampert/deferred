
#include "test_include.glsl"

// Vertex inputs, in model space:
VS_IN_POSITION  vec3 a_position;
VS_IN_NORMAL    vec3 a_normal;
VS_IN_TANGENT   vec3 a_tangent;
VS_IN_BITANGENT vec3 a_bitangent;
VS_IN_TEXCOORD  vec2 a_texcoords;

// Uniform variables:
uniform mat4 u_mvp_matrix;

//layout(location = 0) out vec3 v_world_pos;
layout(location = 1) out vec3 v_normal;
layout(location = 2) out vec3 v_tangent;
layout(location = 3) out vec3 v_bitangent;
//layout(location = 4) out vec4 v_color;
layout(location = 5) out vec2 v_texcoords;

void main()
{
	v_normal    = a_normal;
	v_tangent   = a_tangent;
	v_bitangent = a_bitangent;
	v_texcoords = a_texcoords;

	gl_Position = u_mvp_matrix * vec4(a_position, 1.0);
}
