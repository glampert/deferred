
layout(location = 0) in mat4 v_inverse_viewproj;

layout(location = 0) out vec3 rt_diffuseLight;
layout(location = 1) out vec3 rt_specularLight;

uniform sampler2D u_depth_samp;  // tmu:0
uniform sampler2D u_normal_samp; // tmu:1

uniform vec3 u_camera_world_pos;

const float pointLightRadius    = 2.5;
const float pointAttenConst     = 1.0;
const float pointAttenLinear    = 2.0 / pointLightRadius;
const float pointAttenQuadratic = 1.0 / (pointLightRadius * pointLightRadius);

const vec3 lightWorldPosition = vec3(4.5, 2.5, -8.5);
const vec4 lightColor = vec4(1.0);

const vec2 inv_screen_dimensions = vec2(1.0 / 1024.0, 1.0 / 768.0);

/*
float clamp(float x, float minVal, float maxVal)
{
	return min(max(x, minVal), maxVal);
}
*/

void main()
{
	vec3 fragment_pos = vec3(gl_FragCoord.xy * inv_screen_dimensions, 0.0);
	fragment_pos.z = texture(u_depth_samp, fragment_pos.xy).r;

	vec3 normal = normalize((texture(u_normal_samp, fragment_pos.xy).rgb * 2.0) - 1.0);

	vec4 clip = v_inverse_viewproj * vec4((fragment_pos * 2.0) - 1.0, 1.0);

	fragment_pos = clip.xyz / clip.w;

	float dist = length(lightWorldPosition - fragment_pos); //TODO try distance() built-in!

	float atten = 1.0 - clamp(dist / pointLightRadius, 0.0, 1.0);

	if (atten <= 0.0)
	{
		discard;
	}

	vec3 incident = normalize(lightWorldPosition - fragment_pos);

	vec3 view_dir = normalize(u_camera_world_pos - fragment_pos);

	vec3 half_dir = normalize(incident + view_dir);

	float lambert = clamp(dot(incident, normal), 0.0, 1.0);

	float rfactor = clamp(dot(half_dir, normal), 0.0, 1.0);

	float sfactor = pow(rfactor, 33.0);

	rt_diffuseLight = lightColor.xyz * lambert * atten;

	rt_specularLight = lightColor.xyz * sfactor * atten * 0.33;

// DEBUG VISUALIZATION OF THE MAPS GENERATED BY THE GEOMETRY PASS:
//	rt_diffuseLight  = texture(u_depth_samp,  fragment_pos.xy).rgb;
//	rt_specularLight = texture(u_normal_samp, fragment_pos.xy).rgb;
}
