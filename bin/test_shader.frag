
uniform sampler2D u_diffuse_samp; // tmu: 0

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_texcoords;

// Final color written to frame-buffer:
//out vec4 fragColor;

layout(location = 0) out vec3 rt_position;
layout(location = 1) out vec3 rt_diffuse;
layout(location = 2) out vec3 rt_normal;
layout(location = 3) out vec3 rt_texcoords;

void main()
{
	rt_position  = v_position; // or ( v_position / max_scene_extents )
	rt_diffuse   = texture(u_diffuse_samp, v_texcoords).rgb;
	rt_normal    = v_normal;
	rt_texcoords = vec3(v_texcoords, 0.0);

//	fragColor = texture(u_diffuse_samp, v_texcoords);

	/*
	const vec3 light = normalize(vec3(0.0, 0.0, -1.0));

	float ambient = 0.3;
	float diffuse = max(0, dot(v_normal, light));

	fragColor = vec4(vec3(ambient + 0.5 * diffuse), 1.0);
	*/

//	fragColor = vec4(0.0, 0.7, 0.7, 1.0);
}
