
uniform mat4 u_mvp_matrix;
uniform mat4 u_viewproj_matrix;

layout(location = 0) in vec3 a_position;

layout(location = 0) out mat4 v_inverse_viewproj;

void main()
{
	gl_Position = u_mvp_matrix * vec4(a_position, 1.0);

	//TODO this can be computed by the application!
	v_inverse_viewproj = inverse(u_viewproj_matrix);
}
