
uniform sampler2D u_diffuse_color_samp;  // tmu:0
uniform sampler2D u_light_emissive_samp; // tmu:1
uniform sampler2D u_light_specular_samp; // tmu:2

layout(location = 0) in vec2 v_texcoords;

out vec4 frag_color;

void main()
{
	vec3 diffuse_color = texture(u_diffuse_color_samp, v_texcoords).rgb;

	vec3 light_emissive = texture(u_light_emissive_samp, v_texcoords).rgb;

	vec3 light_specular = texture(u_light_specular_samp, v_texcoords).rgb;

	frag_color.rgb  = diffuse_color * 0.2; // ambient
	frag_color.rgb += diffuse_color * light_emissive; // lambert
	frag_color.rgb += light_specular; // Specular
	frag_color.a = 1.0;
}
