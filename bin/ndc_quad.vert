
// xy = vertex position in normalized device coordinates ([-1,+1] range).
layout(location = 0) in vec2 a_vertex_position_ndc;

// Output tex coordinates:
layout(location = 0) out vec2 v_texcoords;

void main()
{
	// Scale vertex attribute to [0,1] range, producing valid UVs:
	const vec2 scale = vec2(0.5, 0.5);
	v_texcoords = a_vertex_position_ndc * scale + scale;
	gl_Position = vec4(a_vertex_position_ndc, 0.0, 1.0);
}
